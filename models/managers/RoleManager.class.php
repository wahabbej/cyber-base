<?php 
class RoleManager
{
    public function afficherTout(){
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from role');
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "Role");
            $sql->execute();
            $resultats= $sql->fetchAll();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
}