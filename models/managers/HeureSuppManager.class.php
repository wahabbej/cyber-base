<?php 
class HeureSuppManager
{
    public function afficherTout()
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from heuresupp ');
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "HeureSupp");
            $sql->execute();
            $resultat= $sql->fetchAll();
            return $resultat;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        } 
    }

    public function getElementByIdSalarie($idSalarie)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from heuresupp where idSalarie=:idSalarie  order by date');
            $sql->bindValue(':idSalarie',$idSalarie);
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "HeureSupp");
            $sql->execute();
            $resultat= $sql->fetchAll();
            return $resultat;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }


    public function getElementByIdHeureSupp($idHeureSupp)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * FROM `heuresupp` WHERE idHeureSupp= :idHeureSupp');
            $sql->bindValue(':idHeureSupp',$idHeureSupp);
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "HeureSupp");
            $sql->execute();
            $resultat= $sql->fetch();
            return $resultat;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }



    public function ajoutHeureSupp(HeureSupp $heureSupp)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('INSERT INTO `heuresupp`(`date`, `heureDebut`, `heureFin`, `nbMinute`, `idSalarie`) VALUES (:date,:heureDebut,:heureFin,:nbMinute,:idSalarie)');
            $sql->bindValue(':date',$heureSupp->getDate());
            $sql->bindValue(':heureDebut',$heureSupp->getHeureDebut());
            $sql->bindValue(':heureFin',$heureSupp->getHeureFin());
            $sql->bindValue(':nbMinute',$heureSupp->getNbMinute());
            $sql->bindValue(':idSalarie',$heureSupp->getIdSalarie());
            $resultats=$sql->execute();
             
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function modifierHeureSupp(HeureSupp $heureSupp)
   {
    try{
        $sql=CyberPdo::getInstance()->prepare('UPDATE `heuresupp` SET `date`=:date,`heureDebut`=:heureDebut,`heureFin`= :heureFin,`nbMinute`=:nbMinute,`idSalarie`= :idSalarie WHERE idHeureSupp=:idHeureSupp');
        $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "CongeAcquis");
        $sql->bindValue(':date',$heureSupp->getDate());
        $sql->bindValue(':heureDebut',$heureSupp->getHeureDebut());
        $sql->bindValue(':heureFin',$heureSupp->getHeureFin());
        $sql->bindValue(':nbMinute',$heureSupp->getNbMinute());
        $sql->bindValue(':idSalarie',$heureSupp->getIdSalarie());
        $sql->bindValue(':idHeureSupp',$heureSupp->getIdHeureSupp());
        $resultats=$sql->execute();
         
        return $resultats;
    }catch(PDOException $e)
    {
        echo $e->getMessage();
    }

   }

   public function getSumHeureSupp($idSalarie)
   {
    try{
        $sql=CyberPdo::getInstance()->prepare('SELECT sum(nbMinute) as somme FROM `heuresupp` WHERE idSalarie=:idSalarie ') ;
        $sql->bindValue(':idSalarie',$idSalarie);
        $sql->execute();
        $resultat= $sql->fetch(PDO::FETCH_OBJ);
        return $resultat;

    }catch(PDOException $e)
        {
            echo $e->getMessage();
        }


    }

    public function supprimerHeureSupp($idHeureSupp,$idSalarie)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('DELETE FROM `heuresupp` WHERE idHeureSupp=:idHeureSupp and idSalarie= :idSalarie');
            $sql->bindValue(':idHeureSupp',$idHeureSupp);
            $sql->bindValue(':idSalarie',$idSalarie);
            $resultats = $sql->execute();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

   

}