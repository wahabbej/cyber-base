<?php
class SalarieManager
{
    public function verifier($login)
    {
        try
        { 
            $sql= CyberPdo::getInstance()->prepare("SELECT * FROM salarie where email=:email ");
            $sql->bindValue(':email',$login);
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "Salarie");
            $sql->execute();
            $resultat= $sql->fetchAll();
            return $resultat;
            
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function afficherTout()
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from salarie');
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "salarie");
            $sql->execute();
            $resultats= $sql->fetchAll();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public static function deconnecter()
    {
        if(isset($_SESSION["autorisation"]))
        {
        unset($_SESSION["autorisation"]);

        }
        if(isset($_SESSION['idSalarie']))
        {
            unset($_SESSION['idSalarie']);
        }

    }

    public function getElementById($idSalarie)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from salarie where idSalarie=:idSalarie');
            $sql->bindValue(':idSalarie',$idSalarie);
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "salarie");
            $sql->execute();
            $resultats= $sql->fetch();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function modifierSalarie(Salarie $salarie)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('UPDATE `salarie` SET `nom`=:nom,`prenom`=:prenom,`email`=:email,`tauxHoraire`=:tauxHoraire,`nbJourSemaine`=:nbJourSemaine,`idRole`=:idRole WHERE idSalarie = :idSalarie');
            $sql->bindValue(':idSalarie',$salarie->getIdSalarie());
            $sql->bindValue(':nom',$salarie->getNom());
            $sql->bindValue(':prenom',$salarie->getPrenom());
            $sql->bindValue(':email',$salarie->getEmail());
            $sql->bindValue(':tauxHoraire',$salarie->getTauxHoraire());
            $sql->bindValue(':nbJourSemaine',$salarie->getNbJourSemaine());
            $sql->bindValue(':idRole',$salarie->getIdRole());
            $resultats=$sql->execute();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function ajoutSalarie(Salarie $salarie)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('INSERT INTO salarie(nom, prenom, email, mdp,tauxHoraire,nbJourSemaine, idRole) VALUES (:nom,:prenom,:email,:mdp,:tauxHoraire,:nbJourSemaine,:idRole)');
            $sql->bindValue(':nom',$salarie->getNom());
            $sql->bindValue(':prenom',$salarie->getPrenom());
            $sql->bindValue(':email',$salarie->getEmail());
            $sql->bindValue(':mdp',$salarie->getMdp());
            $sql->bindValue(':tauxHoraire',$salarie->getTauxHoraire());
            $sql->bindValue(':nbJourSemaine',$salarie->getNbJourSemaine());
            $sql->bindValue(':idRole',$salarie->getIdRole()); 
            $resultats=$sql->execute();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function supprimerSalarie($id)
    {
        try{           
            $connex=CyberPdo::getInstance();
            $connex->beginTransaction();

            $sql=$connex->prepare('DELETE FROM congeacquis WHERE idSalarie=:id');
            $sql->bindValue(':id',$id);
            $sql->execute();

            $sql2=$connex->prepare('DELETE FROM salarie_conge WHERE idSalarie=:id');
            $sql2->bindValue(':id',$id);
            $sql2->execute();
            $sql3=$connex->prepare('DELETE FROM heuresupp WHERE idSalarie=:id');
            $sql3->bindValue(':id',$id);
            $sql3->execute();

            $sql1=$connex->prepare('DELETE FROM salarie WHERE idSalarie=:id');
            $sql1->bindValue(':id',$id);
            $sql1->execute();
            $connex->commit();
            return true;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
            $connex->rollBack();
            return false;

        }
    }

    public function afficherToutapi()
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from salarie');
            $sql->execute();
            $resultats= $sql->fetchAll(PDO::FETCH_ASSOC);
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    

   
    

}