<?php
class CongeAcquisManager
{
    public function afficherTout()
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from congeAcquis where statut=1  ');
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "CongeAcquis");
            $sql->execute();
            $resultats= $sql->fetchAll();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    // public function afficherToutMiseNiveau()
    // {
    //     try{
    //         $sql=CyberPdo::getInstance()->prepare('SELECT * from congeAcquis');
    //         $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "CongeAcquis");
    //         $sql->execute();
    //         $resultats= $sql->fetchAll();
    //         return $resultats;
    //     }catch(PDOException $e)
    //     {
    //         echo $e->getMessage();
    //     }
    // }

    public function getElementById($id)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from congeAcquis where idCongeAcquis=:id');
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "CongeAcquis");
            $sql->bindValue(':id',$id);
            $sql->execute();
            $resultats= $sql->fetch();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }
    

    public function getElementByIdSalarie($idSalarie)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from congeacquis NATURAL join conge NATURAL join salarie  where idSalarie=:idSalarie and statut=1  order by datedebut');
            $sql->bindValue(':idSalarie',$idSalarie);
            $sql->execute();
            $resultats= $sql->fetchAll(PDO::FETCH_OBJ);
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }

    } 

    public function ajoutCongeAcquis(CongeAcquis $congeAcquis)
   {
    try{
        $sql=CyberPdo::getInstance()->prepare('INSERT INTO `congeacquis`(`dateDebut`, `dateFin`, `acquis`,statut, `idSalarie`, `idConge`) VALUES (:dateDebut,:dateFin,:acquis,:statut,:idSalarie,:idConge)');
        $sql->bindValue(':dateDebut',$congeAcquis->getDateDebut());
        $sql->bindValue(':dateFin',$congeAcquis->getdateFin());
        $sql->bindValue(':acquis',$congeAcquis->getAcquis());
        $sql->bindValue(':idSalarie',$congeAcquis->getIdSalarie());
        $sql->bindValue(':idConge',$congeAcquis->getIdConge());
        $sql->bindValue(':statut',$congeAcquis->getStatut());
        $resultats=$sql->execute();
         
        return $resultats;
    }catch(PDOException $e)
    {
        echo $e->getMessage();
    }

   }

   public function afficherCongeJoinPris($id)
   {
    try{
        $sql=CyberPdo::getInstance()->prepare('SELECT idSalarie, conge.idConge, conge.code,conge.jourAttribue,sum(acquis) as acquis,conge.codeCouleur FROM conge NATURAL join congeacquis   WHERE idSalarie =:idSalarie and statut=1  GROUP by congeacquis.idConge  ');
        $sql->bindValue(':idSalarie',$id);
        $sql->execute();
        $resultat= $sql->fetchAll(PDO::FETCH_OBJ);
        return $resultat;
    }catch(PDOException $e)
    {
        echo $e->getMessage();
    }

   }

    
   public function modifierCongAcquis(CongeAcquis $congeAcquis)
   {
    try{
        $sql=CyberPdo::getInstance()->prepare('UPDATE `congeacquis` SET `dateDebut`=:dateDebut,`dateFin`=:dateFin,`acquis`=:acquis,statut= :statut,`idSalarie`=:idSalarie,`idConge`=:idConge WHERE idCongeAcquis=:idCongeAcquis');
        $sql->bindValue(':idCongeAcquis',$congeAcquis->getIdCongeAcquis());
        $sql->bindValue(':dateDebut',$congeAcquis->getDateDebut());
        $sql->bindValue(':dateFin',$congeAcquis->getdateFin());
        $sql->bindValue(':acquis',$congeAcquis->getAcquis());
        $sql->bindValue(':idSalarie',$congeAcquis->getIdSalarie());
        $sql->bindValue(':idConge',$congeAcquis->getIdConge());
        $sql->bindValue(':statut',$congeAcquis->getStatut());
        $resultats=$sql->execute();
         
        return $resultats;
    }catch(PDOException $e)
    {
        echo $e->getMessage();
    }

   }

   public function supprimerCongeAcquis($idCongeAcquis)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('DELETE FROM `congeacquis` WHERE idCongeAcquis=:idCongeAcquis');
            $sql->bindValue(':idCongeAcquis',$idCongeAcquis);
            $resultats = $sql->execute();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getElementByIdSalarieIdconge($idSalarie,$idConge)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from congeacquis WHERE idSalarie= :idSalarie and idConge=:idConge');
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "CongeAcquis");
            $sql->bindValue(':idSalarie',$idSalarie);
            $sql->bindValue(':idConge',$idConge);
            $sql->execute();
            $resultats= $sql->fetchAll();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getInvalideConge()
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from congeAcquis where statut=0 and archive= 0 ');
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "CongeAcquis");
            $sql->execute();
            $resultats= $sql->fetchAll();
            return $resultats ;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getInvalideCongeJoinSalarie()
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from salarie  inner join congeAcquis on congeAcquis.idSalarie = salarie.idSalarie inner join conge on conge.idConge=congeAcquis.idConge  where statut=0 and archive=0 ');
            $sql->execute();
            $resultats= $sql->fetchAll(PDO::FETCH_OBJ);
            return $resultats ;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function accepterConge($idCongeAcquis)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('UPDATE `congeacquis` SET statut= 1 WHERE idCongeAcquis=:idCongeAcquis');
            $sql->bindValue(':idCongeAcquis',$idCongeAcquis);
           
            $resultats=$sql->execute();
             
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function afficherCongesSalarie($idSalarie)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from  congeAcquis  where idSalarie=:idSalarie and archive=0 ');
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "CongeAcquis");
            $sql->bindValue(':idSalarie',$idSalarie);
            $sql->execute();
            $resultats= $sql->fetchAll();
            return $resultats ;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }

    }

    public function miseAJourAnuelle()
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('UPDATE `congeacquis` SET statut=0,archive= 1 ');
            $resultats=$sql->execute();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function afficherHistorique($idSalarie)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from  congeAcquis  where idSalarie=:idSalarie and archive=1  order by dateDebut ');
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "CongeAcquis");
            $sql->bindValue(':idSalarie',$idSalarie);
            $sql->execute();
            $resultats= $sql->fetchAll();
            return $resultats ;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }

    }


}



