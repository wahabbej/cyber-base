<?php 
class SalarieCongeManager
{
    public function afficherTout()
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from salarie_conge ');
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "SalarieConge");
            $sql->execute();
            $resultats= $sql->fetchAll();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }


    public function ajoutCumuleJour(SalarieConge $salarieConge)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('INSERT INTO `salarie_conge`(`idSalarie`, `idConge`, `cumule`) VALUES (:idSalarie,:idConge,:cumule)');
            $sql->bindValue(':idSalarie',$salarieConge->getIdSalarie());
            $sql->bindValue(':idConge',$salarieConge->getIdConge());
            $sql->bindValue(':cumule',$salarieConge->getCumule());
            $resultats=$sql->execute();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getElementByIdSalarie($idSalarie)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from salarie_conge where idSalarie=:idSalarie');
            $sql->bindValue(':idSalarie',$idSalarie);
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "SalarieConge");
            $sql->execute();
            $resultats= $sql->fetchAll();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function getElementByIdSalarieIdconge($idSalarie,$idConge)
    {
        
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from salarie_conge where idSalarie=:idSalarie and idConge=:idConge');
            $sql->bindValue(':idSalarie',$idSalarie);
            $sql->bindValue(':idConge',$idConge);
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "SalarieConge");
            $sql->execute();
            $resultat= $sql->fetch();
            return $resultat->getCumule();
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }


    public function modifierCumuleJour(SalarieConge $salarieConge)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('UPDATE `salarie_conge` SET `cumule`=:cumule WHERE idSalarie=:idSalarie and idConge=:idConge ');
            $sql->bindValue(':idSalarie',$salarieConge->getIdSalarie());
            $sql->bindValue(':idConge',$salarieConge->getIdConge());
            $sql->bindValue(':cumule',$salarieConge->getCumule());
            $resultats = $sql->execute();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }


    public function affichageCumuleCongeJoinCongeSalarie($idSalarie)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT conge.idConge, conge.code,conge.codeCouleur,salarie_conge.cumule from salarie NATURAL JOIN conge NATURAL JOIN salarie_conge WHERE idSalarie=:idSalarie');
            $sql->bindValue(':idSalarie',$idSalarie);
            $sql->execute();
            $resultats= $sql->fetchAll(PDO::FETCH_OBJ);
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }


    public function getElementByIdSalarieCodeConge($idSalarie,$code)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT conge.idConge,conge.code,conge.codeCouleur,salarie_conge.cumule from salarie NATURAL JOIN conge NATURAL JOIN salarie_conge WHERE idSalarie=:idSalarie AND conge.code= :code');
            $sql->bindValue(':idSalarie',$idSalarie);
            $sql->bindValue(':code', strtoupper($code));
            $sql->execute();
            $resultats= $sql->fetch(PDO::FETCH_OBJ);
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }

    public function modifierCumuleJourJoinConge($idSalarie,$code,$cumule)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('UPDATE `salarie_conge`NATURAL JOIN conge SET salarie_conge.cumule=:cumule WHERE idSalarie=:idSalarie and CODE= :code ');
            $sql->bindValue(':idSalarie',$idSalarie);
            $sql->bindValue(':code',strtoupper($code));
            $sql->bindValue(':cumule',$cumule);
            $resultats = $sql->execute();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }


    public function getElementByIdConge($idConge)
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from salarie_conge where idConge=:idConge');
            $sql->bindValue(':idConge',$idConge);
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "SalarieConge");
            $sql->execute();
            $resultats= $sql->fetchAll();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
    }


    public function miseANiveauRELIQUAT()
    {
        try{
            $sql=CyberPdo::getInstance()->prepare('UPDATE `salarie_conge` SET `cumule`=0 WHERE idConge=9');
            $resultats = $sql->execute();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }

    }
   
}