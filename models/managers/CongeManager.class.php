<?php
class CongeManager
{
   public function afficherTout()
   {
        try{
            $sql=CyberPdo::getInstance()->prepare('SELECT * from conge');
            $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "Conge");
            $sql->execute();
            $resultats= $sql->fetchAll();
            return $resultats;
        }catch(PDOException $e)
        {
            echo $e->getMessage();
        }
   }


   public function ajoutConge(Conge $conge)
   {
    try{
        $sql=CyberPdo::getInstance()->prepare('INSERT INTO `conge`(`libelle`, `code`, `codeCouleur`, `jourAttribue`) VALUES (:libelle,:code,:codeCouleur,:jourAttribue)');
        $sql->bindValue(':libelle',$conge->getLibelle());
        $sql->bindValue(':code',$conge->getCode());
        $sql->bindValue(':codeCouleur',$conge->getCodeCouleur());
        $sql->bindValue(':jourAttribue',$conge->getJourAttribue());
        $resultats = $sql->execute();
        return $resultats;
    }catch(PDOException $e)
    {
        echo $e->getMessage();
    }
   }

   public function getElementById($id)
   {
    try{
        $sql=CyberPdo::getInstance()->prepare('SELECT * from conge WHERE idConge = :idConge');
        $sql->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, "Conge");
        $sql->bindValue(':idConge',$id);
        $sql->execute();
        $resultats= $sql->fetch();
        return $resultats;
    }catch(PDOException $e)
    {
        echo $e->getMessage();
    }
   }

   public function modiffierConge(Conge $conge)
   {
    try{
        $sql=CyberPdo::getInstance()->prepare('UPDATE `conge` SET `libelle`=:libelle,`code`=:code,`codeCouleur`=:codeCouleur,`jourAttribue`= :jourAttribue WHERE idConge= :idConge');
        $sql->bindValue(':idConge',$conge->getIdConge());
        $sql->bindValue(':libelle',$conge->getLibelle());
        $sql->bindValue(':code',$conge->getCode());
        $sql->bindValue(':codeCouleur',$conge->getCodeCouleur());
        $sql->bindValue(':jourAttribue',$conge->getJourAttribue());
        $resultats = $sql->execute();
        return $resultats;
    }catch(PDOException $e)
    {
        echo $e->getMessage();
    } 
   }

   public function supprimerConge($id)
    {
        try{           
            $connex=CyberPdo::getInstance();
            $connex->beginTransaction();

            $sql=$connex->prepare('DELETE FROM congeacquis WHERE idConge=:id');
            $sql->bindValue(':id',$id);
            $sql->execute();

            $sql=$connex->prepare('DELETE  FROM salarie_conge WHERE idConge=:id');
            $sql->bindValue(':id',$id);
            $sql->execute();

            $sql1=$connex->prepare('DELETE  FROM conge WHERE idConge=:id');
            $sql1->bindValue(':id',$id);
            $sql1->execute();
            $connex->commit();
            return true;
    }catch(PDOException $e)
    {
        echo $e->getMessage();
        $connex->rollBack();
        return false;

    }
    }
   
}