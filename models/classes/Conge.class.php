<?php
class Conge 
{
    private $idConge;
    private $libelle;
    private $code;
    private $codeCouleur;
    private $jourAttribue;

    public function hydrate($libelle,$code,$codeCouleur,$jourAttribue)
    {
        $this->setLibelle($libelle);
        $this->setCode($code);
        $this->setCodeCouleur($codeCouleur);
        $this->setJourAttribue($jourAttribue);
    }

    /**
     * Get the value of libelle
     */ 
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set the value of libelle
     *
     * @return  self
     */ 
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get the value of code
     */ 
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set the value of code
     *
     * @return  self
     */ 
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get the value of codeColeur
     */ 
    public function getCodeCouleur()
    {
        return $this->codeCouleur;
    }

    /**
     * Set the value of codeColeur
     *
     * @return  self
     */ 
    public function setCodeCouleur($codeCouleur)
    {
        $this->codeCouleur = $codeCouleur;

        return $this;
    }

    /**
     * Get the value of jourAttribue
     */ 
    public function getJourAttribue()
    {
        return $this->jourAttribue;
    }

    /**
     * Set the value of jourAttribue
     *
     * @return  self
     */ 
    public function setJourAttribue($jourAttribue)
    {
        $this->jourAttribue = $jourAttribue;

        return $this;
    }

    /**
     * Get the value of idConge
     */ 
    public function getIdConge()
    {
        return $this->idConge;
    }

    /**
     * Set the value of idConge
     *
     * @return  self
     */ 
    public function setIdConge($idConge)
    {
        $this->idConge = $idConge;

        return $this;
    }
}