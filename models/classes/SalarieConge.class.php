<?php 
class SalarieConge
{
    private $idSalarie;
    private $idConge;
    private $cumule;
    

    public function hydrate($idSalarie,$idConge,$cumule)
    {
        $this->setIdSalarie($idSalarie);
        $this->setIdConge($idConge);
        $this->setCumule($cumule);
    }

    /**
     * Get the value of idSalarie
     */ 
    public function getIdSalarie()
    {
        return $this->idSalarie;
    }

    /**
     * Set the value of idSalarie
     *
     * @return  self
     */ 
    public function setIdSalarie($idSalarie)
    {
        $this->idSalarie = $idSalarie;

        return $this;
    }

    /**
     * Get the value of idConge
     */ 
    public function getIdConge()
    {
        return $this->idConge;
    }

    /**
     * Set the value of idConge
     *
     * @return  self
     */ 
    public function setIdConge($idConge)
    {
        $this->idConge = $idConge;

        return $this;
    }

    /**
     * Get the value of cumule
     */ 
    public function getCumule()
    {
        return $this->cumule;
    }

    /**
     * Set the value of cumule
     *
     * @return  self
     */ 
    public function setCumule($cumule)
    {
        $this->cumule = $cumule;

        return $this;
    }
}