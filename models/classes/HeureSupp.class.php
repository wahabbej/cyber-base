<?php 
class HeureSupp
{
    private $idHeureSupp;
    private $date;
    private $heureDebut;
    private $heureFin;
    private $nbMinute;
  
    private $idSalarie;


    public function hydrate($date,$heureDebut,$heureFin,$nbMinute,$idSalarie)
    {
        $this->setDate($date);
        $this->setHeureDebut($heureDebut);
        $this->setHeureFin($heureFin);
        $this->setNbMinute($nbMinute);
        $this->setIdSalarie($idSalarie);
    }

    /**
     * Get the value of idHeureSupp
     */ 
    public function getIdHeureSupp()
    {
        return $this->idHeureSupp;
    }

    /**
     * Set the value of idHeureSupp
     *
     * @return  self
     */ 
    public function setIdHeureSupp($idHeureSupp)
    {
        $this->idHeureSupp = $idHeureSupp;

        return $this;
    }

    /**
     * Get the value of date
     */ 
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set the value of date
     *
     * @return  self
     */ 
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get the value of heureDebut
     */ 
    public function getHeureDebut()
    {
        return $this->heureDebut;
    }

    /**
     * Set the value of heureDebut
     *
     * @return  self
     */ 
    public function setHeureDebut($heureDebut)
    {
        $this->heureDebut = $heureDebut;

        return $this;
    }

    /**
     * Get the value of heureFin
     */ 
    public function getHeureFin()
    {
        return $this->heureFin;
    }

    /**
     * Set the value of heureFin
     *
     * @return  self
     */ 
    public function setHeureFin($heureFin)
    {
        $this->heureFin = $heureFin;

        return $this;
    }

    /**
     * Get the value of nbMinute
     */ 
    public function getNbMinute()
    {
        return $this->nbMinute;
    }

    /**
     * Set the value of nbMinute
     *
     * @return  self
     */ 
    public function setNbMinute($nbMinute)
    {
        $this->nbMinute = $nbMinute;

        return $this;
    }


    /**
     * Get the value of idSalarie
     */ 
    public function getIdSalarie()
    {
        return $this->idSalarie;
    }

    /**
     * Set the value of idSalarie
     *
     * @return  self
     */ 
    public function setIdSalarie($idSalarie)
    {
        $this->idSalarie = $idSalarie;

        return $this;
    }
}