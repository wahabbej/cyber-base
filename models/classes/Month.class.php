<?php

class Month
{
     public $days=['Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','dimanche'];
     public  $months = ['Janvier', 'Fevrier', 'Mars', 'Avril', 'Mai','Juin', 'Juillet', 'Aout', 'Septembre','Octobre', 'Novembre', 'Decembre'];

     public function getAll($year)
          {
               $r = [];
               $date = new DateTime($year.'-01-01');
               while($date->format('Y')<=$year)
               {
               $y = $date->format('Y');
               $m = $date->format('m');
               $d = $date->format('d');
               $w= str_replace('0','7', $date->format('w'));
               $r[$y][$m][$d]=$w;
               $date ->add(new DateInterval('P1D'));
               }
               return $r;
          }


     public function nextYear($year)
          {
               $year= $year+1;
               return $year;
          }


     public function previewYear($year)
          {
               $year= $year-1;
               return $year;
          }


     public function getJourFerie($year)
          {
               if(file_exists('src/document/jourFerie'.$year.'.txt')==false)

               {
                    $donnees = @file_get_contents('https://calendrier.api.gouv.fr/jours-feries/metropole/'.$year.'.json');
                    if($donnees===false)
                    {
                         header('location:index.php?path=salarie&action=404');
                         return;
                    }
                    $dossierJourF = 'src/document/jourFerie'.$year.'.txt';
                    file_put_contents($dossierJourF,$donnees);
               }
               $dossierJourF = 'src/document/jourFerie'.$year.'.txt';
               $donnee = fopen($dossierJourF,'r+');
               $jourferier= fread($donnee,filesize($dossierJourF)) ;
               $jourferier=json_decode($jourferier);
               return $jourferier;
          }

     
     public function getDatebetween($year1,$year2)
          {
               $r = [];
               $date = new DateTime($year1.'-01-01');
               while($date->format('Y')<=$year2)
               {
               $y = $date->format('Y');
               $m = $date->format('m');
               $d = $date->format('d');
               $w= str_replace('0','7', $date->format('w'));
               $r[$y][$m][$d]=$w;
               $date ->add(new DateInterval('P1D'));
               }
               return $r;
          }
}
?>
