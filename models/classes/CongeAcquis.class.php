<?php
class CongeAcquis
{
    private $idCongeAcquis;
    private $idSalarie;
    private $idConge;
    private $dateDebut;
    private $dateFin;
    private $acquis;
    private $statut;
    private $archive;

    public function hydrate($idSalarie,$idConge,$dateDebut,$dateFin,$acquis,$statut)
    {
        $this->setIdSalarie($idSalarie);
        $this->setIdConge($idConge);
        $this->setDateDebut($dateDebut);
        $this->setDateFin($dateFin);
        $this->setAcquis($acquis);
        $this->setStatut($statut);
    }

    /**
     * Get the value of idSalarie
     */ 
    public function getIdSalarie()
    {
        return $this->idSalarie;
    }

    /**
     * Set the value of idSalarie
     *
     * @return  self
     */ 
    public function setIdSalarie($idSalarie)
    {
        $this->idSalarie = $idSalarie;

        return $this;
    }

    /**
     * Get the value of idConge
     */ 
    public function getIdConge()
    {
        return $this->idConge;
    }

    /**
     * Set the value of idConge
     *
     * @return  self
     */ 
    public function setIdConge($idConge)
    {
        $this->idConge = $idConge;

        return $this;
    }

    /**
     * Get the value of dateDebut
     */ 
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set the value of dateDebut
     *
     * @return  self
     */ 
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get the value of dateFin
     */ 
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * Set the value of dateFin
     *
     * @return  self
     */ 
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get the value of acquis
     */ 
    public function getAcquis()
    {
        return $this->acquis;
    }

    /**
     * Set the value of acquis
     *
     * @return  self
     */ 
    public function setAcquis($acquis)
    {
        $this->acquis = $acquis;

        return $this;
    }

    /**
     * Get the value of idCongeAcquis
     */ 
    public function getIdCongeAcquis()
    {
        return $this->idCongeAcquis;
    }

    /**
     * Set the value of idCongeAcquis
     *
     * @return  self
     */ 
    public function setIdCongeAcquis($idCongeAcquis)
    {
        $this->idCongeAcquis = $idCongeAcquis;

        return $this;
    }

  

    /**
     * Get the value of statut
     */ 
    public function getStatut()
    {
        return $this->statut;
    }

    /**
     * Set the value of statut
     *
     * @return  self
     */ 
    public function setStatut($statut)
    {
        $this->statut = $statut;

        return $this;
    }

    /**
     * Get the value of archive
     */ 
    public function getArchive()
    {
        return $this->archive;
    }

    /**
     * Set the value of archive
     *
     * @return  self
     */ 
    public function setArchive($archive)
    {
        $this->archive = $archive;

        return $this;
    }
}