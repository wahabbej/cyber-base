<?php
class Salarie 
{
    private $idSalarie;
    private $nom;
    private $prenom;
    private $email;
    private $mdp;
    private $tauxHoraire;
    private $nbJourSemaine;
    private $idRole;

    public function hydrate($nom,$prenom,$email,$tauxHoraire,$nbJourSemaine,$idRole)
    {
        $this->setNom($nom);
        $this->setPrenom($prenom);
        $this->setEmail($email);
        $this->setTauxHoraire($tauxHoraire);
        $this->setNbJourSemaine($nbJourSemaine);
        $this->setIdRole($idRole);
    }
    /**
     * Get the value of nom
     */ 
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */ 
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get the value of prenom
     */ 
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set the value of prenom
     *
     * @return  self
     */ 
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get the value of email
     */ 
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set the value of email
     *
     * @return  self
     */ 
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get the value of mdp
     */ 
    public function getMdp()
    {
        return $this->mdp;
    }

    /**
     * Set the value of mdp
     *
     * @return  self
     */ 
    public function setMdp($mdp)
    {
        $this->mdp = $mdp;

        return $this;
    }

    /**
     * Get the value of idRole
     */ 
    public function getIdRole()
    {
        return $this->idRole;
    }

    /**
     * Set the value of idRole
     *
     * @return  self
     */ 
    public function setIdRole($idRole)
    {
        $this->idRole = $idRole;

        return $this;
    }

   

   

    /**
     * Get the value of idSalarie
     */ 
    public function getIdSalarie()
    {
        return $this->idSalarie;
    }

    /**
     * Set the value of idSalarie
     *
     * @return  self
     */ 
    public function setIdSalarie($idSalarie)
    {
        $this->idSalarie = $idSalarie;

        return $this;
    }


    /**
     * Get the value of tauxHoraire
     */ 
    public function getTauxHoraire()
    {
        return $this->tauxHoraire;
    }

    /**
     * Set the value of tauxHoraire
     *
     * @return  self
     */ 
    public function setTauxHoraire($tauxHoraire)
    {
        $this->tauxHoraire = $tauxHoraire;

        return $this;
    }

    /**
     * Get the value of nbJourSemaine
     */ 
    public function getNbJourSemaine()
    {
        return $this->nbJourSemaine;
    }

    /**
     * Set the value of nbJourSemaine
     *
     * @return  self
     */ 
    public function setNbJourSemaine($nbJourSemaine)
    {
        $this->nbJourSemaine = $nbJourSemaine;

        return $this;
    }
}
