<?php 
class CyberPdo
{
    private static $serveur='mysql:host=localhost';
private static $bdd='dbname=cyber_base'; 
private static $user='root' ; 
private static $mdp='' ;
private static $CyberPdo;
private static $unPdo = null;

//	Constructeur privé, crée l'instance de PDO qui sera sollicitée
//	pour toutes les méthodes de la classe
private function __construct()
{
    CyberPdo::$unPdo = new PDO(CyberPdo::$serveur.';'.CyberPdo::$bdd, CyberPdo::$user, CyberPdo::$mdp);
    CyberPdo::$unPdo->query("SET CHARACTER SET utf8");
    CyberPdo::$unPdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
public function __destruct()
{ 
    CyberPdo::$unPdo = null;
}
/**
*	Fonction statique qui cree l'unique instance de la classe
* Appel : $instanceCyberPdo = CyberPdo::getCyberPdo();
*	@return l'unique objet de la classe CyberPdo
*/
public static function getInstance()
{
    if(self::$unPdo == null)
    {
        self::$CyberPdo= new CyberPdo();
    }
    return self::$unPdo;
}




}
