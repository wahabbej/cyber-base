<?php

use JetBrains\PhpStorm\Internal\ReturnTypeContract;

/**
 * securiser des url est des formulaire d'injection sql
 *
 * @param [type] $donnees 
 * @return void
 */
function securiser($donnees)
{
    $donnees = trim($donnees);

    //enleve les entislashes pour eviter d'ignorer du code 
    $donnees = stripslashes($donnees);
    $donnees = htmlspecialchars($donnees);
    return $donnees;
}



function congeSeul($dateDebut,$dateFin)
{
    $objMonth = new Month();
            $dates=[];
            
            foreach (new DatePeriod(new DateTime($dateDebut), new DateInterval('P1D'), (new DateTime($dateFin))) as $dt)
                {
                    $dates[]=$dt->format('Y-m-d');
                }

            $weekend = [];
            $resultatMonth = $objMonth->getDatebetween((new DateTime($dateDebut))->format('Y') ,(new DateTime($dateFin))->format('Y'));
            foreach($resultatMonth as $year =>$mois)
            {
                foreach($mois as $moi=>$jours){
                    foreach($jours as $d=>$w){
                        if($w==6||$w==7){
                            $weekend[]  =  $year.'-'.$moi.'-' .$d ;
                        }
                    }
                }
            }
               
            foreach ($weekend as $unWeekEnd)
            {
                if (($date = array_search($unWeekEnd, $dates)) !== false)
                {
                    unset($dates[$date]);
                }
            }
            
            $jourFeries=[];
            $resultatDebut = $objMonth->getJourFerie((new DateTime($dateDebut))->format('Y') );
            $resultatFin = $objMonth->getJourFerie((new DateTime($dateFin))->format('Y'));
            foreach( $resultatDebut as $cle =>$valeur)
                {
                    $jourFeries[]=$cle;
                }
            foreach( $resultatFin as $cle =>$valeur)
                {
                        $jourFeries[]=$cle;
                }
            foreach ($jourFeries as $jourFerie)
            {
                if (($date = array_search($jourFerie, $dates)) !== false)
                {
                    unset($dates[$date]);
                }
            }
           
            return count($dates );
}

function diffMinute($dateDebut,$dateFin)
    {
        $date1=new DateTime($dateDebut);
        $date2=new DateTime($dateFin);  
        $dates = [];
        foreach (new DatePeriod($date1, new DateInterval('PT1M'), $date2) as $dt)
        {
            $dates[]=$dt;
        }
        return count($dates);
    }

function verfDate($date1,$date2)
{
    if((new DateTime($date1))>(new DateTime($date2)))
    {
        return true;
       
    }
}

function transformDate($date)
{
    return (new DateTime($date));
}

function debug($variable)
{
    echo '<pre>';
    print_r($variable) ;
    echo '</pre>';
}
