<nav class="navbar navbar-expand-lg navbar-light bg-light sticky-top">
  <div class="container-fluid  ">
    <a class="navbar-brand" href="#"> <img src="src/images/charte_graphique_cyber_base-01.jpg" width="50" height="50" class="d-inline-block align-top" alt=""></a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class=" collapse navbar-collapse " id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0 ">
        <?php if (isset($_SESSION['autorisation'])&&$_SESSION['autorisation']=='admin'){?>
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="index.php?path=salarie&action=accueiladmin">Accueil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="index.php?path=salarie&action=listeSalarie">Salariés</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="index.php?path=salarie&action=gererCongeSalarie">Congé</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            cumule Congé
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
            <li><a class="dropdown-item" href="index.php?path=salarie&action=gererHeureSupp">Heures Supp</a></li>
            <li><a class="dropdown-item" href="index.php?path=salarie&action=compteEpargne">Compte epargne temps</a></li>
          </ul>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="index.php?path=salarie&action=gererHistotique" > Historique</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="index.php?path=conge&action=listeConge">Catégorie congés</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="index.php?path=salarie&action=planing">Planing </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="index.php?path=salarie&action=editeCongeManuel">Editer manuel</a>
        </li>
        <li class="nav-item">
          <a class="nav-link position-relative mx-5" href="index.php?path=salarie&action=listeCongeSoumis">Congé en attente<span class="position-absolute top-4 start-100 translate-middle badge rounded-pill bg-danger">
            <?php 
                  echo $nbLigne;  ?>
         </span> </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="index.php?path=conge&action=calendrier">Calendrier</a>
        </li>
     </ul>
    <ul class="navbar-nav me-auto mb-2 mb-lg-0 ">
      <?php }?>
      <?php if (isset($_SESSION['autorisation'])&&$_SESSION['autorisation']=='salarie'){?>
        <li class="nav-item">
          <a class="nav-link" href="index.php?path=salarie&action=accueilSalarie">Accueil</a>
        </li> 
        <li class="nav-item">
          <a class="nav-link" href="index.php?path=salarie&action=listeCongeSalarie">Mes congés</a>
        </li> 
        <li class="nav-item">
          <a class="nav-link" href="index.php?path=salarie&action=listeHeureSupp">Mes heures Supp</a>
        </li> 
        <li class="nav-item">
          <a class="nav-link" href="index.php?path=salarie&action=depotConge">Déposer un congé</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="index.php?path=salarie&action=planing">Planing </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="index.php?path=conge&action=calendrier">Calendrier</a>
        </li> 
      </ul>
      <?php }?>
      <?php if(isset($_SESSION['autorisation'])) {?>
        <a class="ml-2 btn btn-danger pull-right" href="index.php?path=salarie&action=deconnexion"><img src="src/images/box-arrow-right.svg" alt="">    Déconnexion</a>
        <?php }?>  
      <?php if(!isset($_SESSION['autorisation']))
      {?>
      <div class="navbar-nav d-flex justify-content-end  me-auto mb-2 mb-lg-0">
        <form novalidate action="index.php?path=salarie&action=connexion" method="POST" class="d-flex">
        <input required class="form-control me-2" type="mail" name="login" placeholder="Email" >
        <input required minlength="" class="form-control me-2" type="password" name="mdp" placeholder="mot de passe" >
        <button class="btn btn-outline-success" type="submit">Connexion</button>
      </form>
      </div>
      <?php } ?>

    </div>
  </div>
</nav>