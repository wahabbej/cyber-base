<?php
$title = 'Cumule congé';
ob_start();
?>
<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Cumule des congés</h1>
    </div>
</div>
<div class="container">
    <?php if(isset($_SESSION['flash'])){
        foreach($_SESSION['flash'] as $class =>$message){
        ?>
        <div class="row d-flex justify-content-center">
            <div class="alert alert-<?=$class?> col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center " id="alerte" role="alert">
                <?php 
                echo $message;
                unset($_SESSION["flash"]);
                ?>
            </div>
        </div>
    <?php }}?>
</div>
<div class="container pb-3 mb-5">
<table class="table">
    <tr class="text-center"><th colspan="3"><?=$resultatSalarie->getNom().' '. $resultatSalarie->getPrenom()?></th></tr>
    <tr><th>Congé</th><th>Cumule</th><th>Action</th></tr>
    <?php foreach($resultatSalarieConges as $resultatSalarieConge) { ?>
        <tr style="background-color: <?= $resultatSalarieConge->codeCouleur?>;">
            <td><?= $resultatSalarieConge->code?></td>
            <td><?= $resultatSalarieConge->cumule?></td>
            <td><a class="btn btn-secondary" href="index.php?path=cumuleConge&action=modifierCumuleConge&code=<?= $resultatSalarieConge->code?>&idSalarie=<?=$idSalarie?>">Modifier</a></td>
        </tr>
    <?php }?>
</table>
</div>

<?php
$content = ob_get_clean();
require('views/template.php');
