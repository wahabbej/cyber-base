<?php
$title = 'Editer congé pris ';
ob_start();
?>
<div class="container d-flex justify-content-center mt-5"><div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom"><h1 class="px-5">Editer compte epargne temps  </h1></div></div>
 <div class="container">
    <?php if(isset($_SESSION['flash'])){
        foreach($_SESSION['flash'] as $class =>$message){
        ?>
        <div class="row d-flex justify-content-center">
            <div class="alert alert-<?=$class?> col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center " id="alerte" role="alert">
                <?php 
                echo $message;
                unset($_SESSION["flash"]);
                ?>
            </div>
        </div>
    <?php }}?>
</div>
<div class="text-center"><h3><?=$resultatSalarie->getPrenom().' ' .$resultatSalarie->getNom()?></h3></div>
<div class="container d-flex justify-content-center mb-5">
    <div class="col-md-12  col-sm-12 col-lg-6 shadow-lg p-5  bg-body rounded">
        <form novalidate class="form" action="index.php?path=cumuleConge&action=confirmAjout" method="POST">
            <label class="form-label mt-4" for="nbJour">Nombre de jour </label>
            <input required class="form-control" type="number" id="nbJour" name="nbJour">
            <label class="form-label mt-4" for="typeConge">Choisir un congé à déduire </label>
            <select required name="idConge" class="form-select">
                <option  selected value="">Choisir la catégorie </option>
                <?php foreach($resultatConges as $resultatConge){ ?>
                <option value="<?= $resultatConge->getIdConge() ?>"><?= $resultatConge->getLibelle() ?></option>
                <?php }?>
            </select>
            <input type="hidden" name="tokenT" value="<?=$token?>">
            <input type="hidden" name="idSalarie" value="<?=$idSalarie?>">
            <div class="row d-flex justify-content-between mt-4 ">
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                    <a class="btn btn-success " href="index.php?path=salarie&action=compteEpargne"><img class="text-success" src="src/images/arrow-left-circle-fill.svg" alt=""></a>
                </div>
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">    
                    <button class="btn btn-warning "  type="submit"><img src="src/images/pencil-square.svg" alt=""></button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php $content = ob_get_clean();
require 'views/template.php';