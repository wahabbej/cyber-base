<?php
$title = 'Modifier cumule congé';
ob_start();
?>
<div class="container d-flex justify-content-center mt-3">
<div class="text-center mt-4 mb-4 shadow-sm p-3  bg-body rounded border-5 border-warning border-end border-bottom">
    <h1 class="px-5"> Modifier cumule conge </h1>
</div>
</div>
<div class="container d-flex justify-content-center">
    <div class="col-md-12  col-sm-12 col-lg-6 shadow-lg p-5 bg-body rounded">
    <form action="index.php?path=cumuleConge&action=confirmerModif" method="POST">
        <div class="row d-flex justify-content-between ">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6  ">
               <h5>Nom de salarié </h5> 
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 text-center ">
                <?=$resultatSalarie->getNom().' '. $resultatSalarie->getPrenom()?>
            </div>
        </div>
        <div class="row d-flex justify-content-between   mt-4">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6  ">
               <h5>Type de congé </h5> 
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 text-center ">
                <?=$resultatSalarieConge->code  ?>
            </div>
            <input type="hidden" name="idSalarie" value="<?= $idSalarie?>">
            <input type="hidden" name="code" value="<?= $code?>">
            
        </div>
        <div class="row d-flex justify-content-between mt-4">
        <div class="col-6 col-sm-6 col-md-6 col-lg-6  ">
               <h5> Cumule</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 text-center  ">
                <input  value="<?= $resultatSalarieConge->cumule ?>" class="form-control rounded-pill text-center" type="number" name="cumule">
            </div>
        </div>
        <div class="row mt-4 d-flex justify-content-start mt-4">
            <div class="col-2 col-sm-2 col-md-2 col-lg-2    ">
            <input type="hidden" name="tokenT" value="<?=$token?>">
                <button  class="btn btn-warning" type="submit"><img src="src/images/pencil-square.svg" alt=""></button>
            </div>
        </div>
    </form>
</div>
<?php
$content = ob_get_clean();
require('views/template.php');
