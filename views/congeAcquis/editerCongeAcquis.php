<?php
$title = 'Editer congé pris ';
ob_start();
$periode = 0;
?>
<div class="container d-flex justify-content-center mt-2">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Editer congé </h1>
    </div>
</div>
<div class="container">
    <?php if(isset($_SESSION['flash'])){
        foreach($_SESSION['flash'] as $class =>$message){
        ?>
        <div class="row d-flex justify-content-center">
            <div class="alert alert-<?=$class?> col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center" id="alerte" role="alert">
                <?php 
                echo $message;
                unset($_SESSION["flash"]);
                ?>
            </div>
        </div>
    <?php }}?>
</div>
<div class=" container ">
        <div class="row ">
            <div id="ajoutConge"  class="col-12 col-sm-12 col-md-6 col-lg-6 shadow-lg  mb-5 bg-body rounded order-2 ">
                <form novalidate action="index.php?path=congeAquis&action=ajoutCongeSalarie" method="POST">
                    <div class="text-center"><h5>Ajouter un nouveau congé</h5></div>
                    <label for="dateDebut" class="form-label">Date début</label>
                    <input required id="dateDebut" type="date" class="form-control" name="dateDebut">
                    <label for="dateFin" class="form-label">Date fin </label>
                    <input required id="dateFin" type="date" class="form-control" name="dateFin">
                    <input id="decomptet" type="hidden" name="decomptet" value="">
                    <input name="idSalarie" value="<?= $resultat->getIdSalarie() ?>" type="hidden">
                    <input type="hidden" name="tokenT" value="<?=$token?>">
                    <label for="decompte" class="form-label">Décompte</label>
                    <div class="text-center form-control" id="decompte"></div>
                    <label for="idConge">Type de congé</label>
                     <select required id="idConge" name="idConge" class="form-select" aria-label="Default select example">
                        <option value="">Choisir le type de congé</option>
                        <?php foreach ($resultatConges as $resultatConge) { ?>
                            <option  value="<?= $resultatConge->getIdConge() ?>"><?= $resultatConge->getLibelle() ?></option>
                        <?php } ?>
                     </select>
                    <button class="btn btn-success mt-2" type="submit">soumettre</button>
                </form>          
            </div>
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 mb-5">
            <table  class="table table-bordered table-congeEdite order-1">
                <tr>
                    <th>Type de congés</th>
                    <th>Jours restants </th>
                    <th>Totale jours Pris</th>
                    <th>Cumule jours</th>
                </tr>
                <?php foreach($resultatConges as $resultatConge){ ?> 
                
                        
                <tr class="<?= $resultatConge->getIdConge() ?>"style="background-color:<?=$resultatConge->getCodeCouleur()?> ;" >
                    <td><?= $resultatConge->getCode()?></td>
                
                        <td id="jourRest<?= $resultatConge->getIdConge() ?>">
                        <?php foreach($resultatSalarieConges as $resultatSalarieConge)
                            {
                                if($resultatSalarieConge->getIdConge()==$resultatConge->getIdConge())
                                { 
                                    echo $reste= $resultatSalarieConge->getCumule();
                                } }
                            ?>
                            
                        </td>
                        <td id="jourPris<?= $resultatConge->getIdConge() ?>"><?php foreach($resultats as $unresultat){
                                                                                            if($unresultat->idConge== $resultatConge->getIdConge())
                                                                                            {                                                       
                                                                                                echo $pris= $unresultat->acquis;
                                                                                            }
                                                                                            
                                                                                        } ?>
                        </td>
                        <td id="attribut<?=$resultatConge->getIdConge() ?>"><?php foreach($resultats as $unresultat)
                                                                                        {
                                                                                            if($unresultat->idConge== $resultatConge->getIdConge()){
                                                                                                echo $reste+$pris;
                                                                                            }
                                                                                        } ?>
                        </td>
                    </tr>
                <?php }?>
            </table>
        
        </div >
    </div>
</div>
<div class="container mb-5">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
            <table  class="table table-stripted table-bordered table-editeConge table-dark table-hover">
                <tr>
                    <th colspan="6" class="text-center"><?= $resultat->getNom() . ' ' . $resultat->getPrenom() ?></th>
                </tr>
                <tr>
                    <th>Periodes</th>
                    <th class="text-center" colspan="2">Dates des congés</th>
                    <th>Décompte Jours </th>
                    <th>type de congé </th>
                    <th>Action</th>
                </tr>
                
                <?php foreach ($resulatCongeAcquis as $resulatCongeAcqui) { ?>
                    <tr class="text-center">
                        <td>Periode <?= $periode = $periode + 1 ?></td>
                        <td><?=  transformDate($resulatCongeAcqui->dateDebut) ->format('d-m-Y') ?></td>
                        <td><?= transformDate($resulatCongeAcqui->dateFin)->format('d-m-Y') ?></td>
                        <td><?= $resulatCongeAcqui->acquis ?></td>
                        <td><?= $resulatCongeAcqui->libelle ?></td>
                        <td><a class="btn btn-dark" href="index.php?path=congeAquis&action=profileCongeAcquis&id=<?= $resulatCongeAcqui->idCongeAcquis ?>&idSalarie=<?=$resulatCongeAcqui->idSalarie?>">Editer</a></td>
                    </tr>
                <?php } ?>
            </table>
        </div>
        
    </div>
</div>
<script src="./src/js/congeedit.js"></script>
<?php $content = ob_get_clean();
require 'views/template.php';
