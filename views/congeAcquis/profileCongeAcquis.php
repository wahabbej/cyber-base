<?php
$title= 'detaile Congé';
ob_start();
?>  
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Datail congé</h1>
    </div>
</div>
<div class="container">
    <?php if(isset($_SESSION['flash'])){
        foreach($_SESSION['flash'] as $class =>$message){
        ?>
        <div class="row d-flex justify-content-center">
            <div class="alert alert-<?=$class?> col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center" id="alerte" role="alert">
                <?php 
                echo $message;
                unset($_SESSION["flash"]);
                ?>
            </div>
        </div>
    <?php }}?>
</div>

<div class="container d-flex justify-content-center">
    <div class="col-12 col-sm-12 col-md-12 col-lg-6 shadow-lg p-5 mb-5 bg-body rounded">   
        <div class="row d-flex justify-content-beteween mt-4">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 ">
                <h5>Salarie :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5><?php foreach($resultatSalaries as $resultatSalarie ){if($resultatSalarie->getIdSalarie()==$resultats->getIdSalarie()){echo $resultatSalarie->getNom();}} ?></h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween mt-4">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Type de congé :</h5>

            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5><?php foreach($resultatConges as $resultatConge){if($resultatConge->getIdConge()==$resultats->getIdConge()){echo $resultatConge->getLibelle(); }} ?></h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween mt-4">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Date debut :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5><?= transformDate($resultats->getDateDebut())->format('d-m-Y') ?></h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween mt-4">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Datefin :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5><?=transformDate($resultats->getdateFin()) ->format('d-m-Y') ?></h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween mt-4 ">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Nombre de jours :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5><?= $resultats->getAcquis()?></h5>
            </div>
        </div>
        <div class="row d-flex justify-content-between mt-5 ">
            <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                <a class="btn btn-success " href="index.php?path=salarie&action=editerconge&id=<?=$resultats->getIdSalarie()?>" ><img class="text-success" src="src/images/arrow-left-circle-fill.svg" alt=""></a>   
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                <a class="btn btn-warning" href="index.php?path=congeAquis&action=modifCongeAcquis&id=<?=$resultats->getIdCongeAcquis()?>"><img  src="src/images/pencil-square.svg" alt=""></a>
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-2 ">
                <form action="index.php?path=congeAquis&action=supprimerCongeAcquis" method="POST">
                    <input type="hidden" value="<?= $resultats->getIdCongeAcquis()?>" name="id">
                    <input type="hidden" value="<?= $resultats->getIdSalarie()?>" name="idSalarie">
                    <input type="hidden" value="<?= $resultats->getIdConge()?>" name="idConge">
                    <input type="hidden" value="<?= $resultats->getAcquis()?>" name="decompte">
                    <input type="hidden" value="<?= $resultats->getDateDebut()?>" name="dateDebut">
                    <input type="hidden" value="<?= $resultats->getDateFin()?>" name="dateFin">
                    <input type="hidden" name="tokenT" value="<?=$token?>">
                    <button type="submit" class="btn btn-danger " ><img  src="src/images/trash3.svg" alt=""></button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $content= ob_get_clean();
require('views/template.php');