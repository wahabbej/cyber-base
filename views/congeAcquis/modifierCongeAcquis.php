<?php
$title= 'Modifier conge acquis';
ob_start();
?>

<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Modifier congé</h1>
    </div>
</div>
<div class="container">
    <?php if(isset($_SESSION['flash'])){
        foreach($_SESSION['flash'] as $class =>$message){
        ?>
        <div class="row d-flex justify-content-center">
            <div class="alert alert-<?=$class?> col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center" id="alerte" role="alert">
                <?php 
                echo $message;
                unset($_SESSION["flash"]);
                ?>
            </div>
        </div>
    <?php }}?>
</div>
 
<div id="ajoutConge"  class="container d-flex justify-content-center ">
<div class="col-md-12  col-sm-12 col-lg-6 shadow-lg p-5 mb-5 bg-body rounded">
    <form action="index.php?path=congeAquis&action=confirmModifCongeAcquis" method="POST">
         <label for="dateDebut" class="form-label mt-4">Date début</label>
            <div><input id="dateDebut" type="date" class="form-control " name="dateDebut" value="<?=transformDate($resultatCongeAquis->getDateDebut())->format('Y-m-d') ?>" ></div>
            <label for="datefin" class="form-label mt-3">Date fin</label>
            <div><input id="dateFin" type="date" class="form-control" name="dateFin" value="<?=transformDate($resultatCongeAquis->getDateFin())->format('Y-m-d') ?>"></div>
            <input id="decomptet" type="hidden"  name="decomptet" value="">
            <input name="idSalarie" value="<?=$resultatCongeAquis->getIdSalarie()?>" type="hidden">
            <div class="mt-3">Décompte jourr</div>
            <div class="" id="decompte"><?=$resultatCongeAquis->getAcquis()?></div>
            <input name="decomptet0" value="<?=$resultatCongeAquis->getAcquis()?>" type="hidden">
            <input type="hidden" name="tokenT" value="<?=$token?>">
            <input type="hidden" name="idCongeAcquis" value="<?=$resultatCongeAquis->getIdCongeAcquis()?>">
            <div>
            <label for="idConge" class="form-label mt-3 borded">Categorie de congé</label>
            <select id="idConge" name="idConge" class="form-select" aria-label="Default select example">
            <?php foreach($resultatConges as $resultatConge){?>
            <option <?php if( $resultatCongeAquis->getIdConge()==$resultatConge->getIdConge()){
                    echo 'selected';
                }
             ?> value="<?=$resultatConge->getIdConge()?>"><?=$resultatConge->getLibelle()?></option>
            <?php } ?>
            </select>
            </div>
            <div class="row d-flex justify-content-between mt-5 ">
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                    <a class="btn btn-success "href="index.php?path=congeAquis&action=profileCongeAcquis&id=<?=$resultatCongeAquis->getIdCongeAcquis()?>&idSalarie=<?=$resultatCongeAquis->getIdSalarie()?>" ><img class="text-success" src="src/images/arrow-left-circle-fill.svg" alt=""></a>  
                </div>
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                    <input type="hidden" value="<?=$resultatCongeAquis->getDateDebut()?>" name="debutAncien">
                    <input type="hidden" value="<?=$resultatCongeAquis->getDateFin()?>" name="finAncien">
                    <input type="hidden" value="<?=$resultatCongeAquis->getIdConge()?>" name="congeAncien">
                    <button class="btn btn-warning " type="submit"><img  src="src/images/pencil-square.svg" alt=""></button>
                </div>
                
            </div>
    </form>
</div>
</div> 
<script  src="./src/js/congeedit.js"></script>
<?php $content= ob_get_clean();
require('views/template.php');