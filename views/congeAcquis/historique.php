<?php
$title= 'detaile heure supp';
ob_start();
$conge = 0;
?> 
<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Historique des congés</h1>
    </div>
</div> 
<div class="text-center"> <h3><?=''.$resultatSalarie->getPrenom().' '.$resultatSalarie->getNom() ?><br><span class="text-danger"><?php if($annee!=0){echo $annee;}?></span></h3></div>
<div class="container">
    <div class="row d-flex justify-content-end">
        <div class="col-12 col-sm-12 col-md-6 col-lg-4">
            <form action="index.php?path=salarie&action=historiqueConge&id=<?=$idSalarie?>" method="POST"  class="d-flex text-end">
                <input class="form-control me-2" type="search" placeholder="Entrer l'année recherché " name="annee">
                <button class="btn btn-outline-success" type="submit">Recherche</button>
            </form>
        </div>
    </div>
    <div class="row">
        <table class="table table-dark table-hover ">
            <tr>
                <th>Congé</th><th>Date debut</th><th>Date fin </th><th>Type de congé </th><th>Décompte</th>
            </tr>
            <?php if($annee ==0) {?>
            <?php foreach($resulatCongeAcquis as $resulatCongeAcqui){?>
            <tr>
                <td><?=$conge=$conge+1?></td>
                <td><?= transformDate( $resulatCongeAcqui->getDateDebut())->format('d-m-Y')?></td>
                <td><?= transformDate($resulatCongeAcqui->getdateFin())->format('d-m-Y') ?></td>
                <td><?php foreach($resulatConges as $resulatConge)
                {
                    if($resulatConge->getIdConge()==$resulatCongeAcqui->getIdConge())
                    {
                        echo $resulatConge->getCode();
                    }
                }
                    ?></td>
                <td><?=$resulatCongeAcqui->getAcquis()?></td>
            </tr>
            <?php }}else{
                foreach($resulatCongeAcquis as $resulatCongeAcqui)
                {
                    if(transformDate( $resulatCongeAcqui->getDateDebut())->format('Y')==$annee){?>
       
        <tr>
                <td><?=$conge=$conge+1?></td>
                <td><?= transformDate( $resulatCongeAcqui->getDateDebut())->format('d-m-Y')?></td>
                <td><?= transformDate($resulatCongeAcqui->getdateFin())->format('d-m-Y') ?></td>
                <td><?php foreach($resulatConges as $resulatConge)
                {
                    if($resulatConge->getIdConge()==$resulatCongeAcqui->getIdConge())
                    {
                        echo $resulatConge->getCode();
                    }
                }
                    ?></td>
                <td><?=$resulatCongeAcqui->getAcquis()?></td>
            </tr>
            <?php }}}?>
         </table>
    </div>
</div>

<?php
$content = ob_get_clean();
require('views/template.php');
