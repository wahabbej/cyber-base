<?php
$title= 'Détail Congé ';
ob_start();
?>

    <?php if (!empty($_SESSION['success'])){?>
        <div class="alert alert-success col-md-4 offset-4" role="alert" id="alerte"><?php 
            echo $_SESSION["success"];
            unset($_SESSION["success"]);
        ?>
        </div>
    <?php }?>
<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Détail congé</h1>
    </div>
</div> 
<div class="container d-flex justify-content-center">
    <div class="col-12 col-sm-12 col-md-12 col-lg-6 shadow-lg p-5 mb-5 bg-body rounded">
        <div class="row d-flex justify-content-beteween d-flex justify-content-beteween ">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Libelle :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5><?= $resultats->getLibelle() ?></h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween d-flex justify-content-beteween mt-4 ">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Code :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5><?=$resultats->getCode() ?></h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween d-flex justify-content-beteween mt-4   ">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Code couleur :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5><?= $resultats->getCodeCouleur()?></h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween d-flex justify-content-beteween mt-4  ">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Nombre de jours attribués :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5><?= $resultats->getJourAttribue()?></h5>
            </div>
        </div>
        <div class="row d-flex justify-content-between mt-5 ">
            <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                <a class="btn btn-success" href="index.php?path=conge&action=listeConge"> <img class="text-success" src="src/images/arrow-left-circle-fill.svg" alt=""></a>
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                <a class="btn btn-warning" href="index.php?path=conge&action=modifConge&id=<?=$resultats->getIdConge()?>"><img  src="src/images/pencil-square.svg" alt=""></a>
            </div>
        </div>
    </div>
</div>
<script src="./src/js/alertProfile.js"></script>
<?php $content= ob_get_clean();
require('views/template.php');