<?php
$title = 'Calendrier ';
ob_start();
?>

<div class="container d-flex justify-content-center mt-4">
    <div class="text-center mt-4 mb-4 shadow-sm  px-5 p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Calendrier</h1>
    </div>
</div>
<div class="container">
    
    <div class=" row d-flex justify-content-center">
    <div class="col-md-1 text-center px-0">
        <div class="btn-group">
            <a class="btn btn-primary" href="index.php?path=conge&action=calendrier&year=<?= $date->previewYear($year) ?>">&lt;</a>
            <a class="btn btn-primary" href="index.php?path=conge&action=calendrier&year=<?= $date->nextYear($year) ?>">&gt;</a>
        </div>
    </div>
    <div class="year mb-3 col-md-4 d-inlin-block  ">
        <form class="d-flex" action="index.php?path=conge&action=calendrier" method="POST">

            <input id="inputYear" class="form-control me-2 text-center ml-2" type="search" name="year" value="<?= $year; ?>" aria-label="Search">
            <button class="btn btn-outline-success" type="submit">Valider</button>
        </form>
    </div>
</div>
    <div class="row  row-calendrier">
        <?php foreach ($datesSeul as $m => $day) { ?>
            <div class=" col-md-2 mx-0 p-0 my-3  table-calendrier">
                <table style="font-size:14px ;" class="table border border-1 col-calendrier ">
                    <tr>
                        <th class="table-dark"><?= $date->months[$m - 1] ?></th>
                    </tr>
                    <?php foreach ($day as $d => $w) { ?>
                        <tr>
                            <td id="td" class="<?php
                                                $couleur = '';
                                                if ($w == 6 || $w == 7) {
                                                    $couleur = 'table-success';
                                                }
                                                foreach ($jourFeries as $dateferie => $libelle) {
                                                    if ($year . '-' . $m . '-' . $d == $dateferie) {
                                                        $couleur = 'table-dark text-danger';
                                                    } elseif ($year . '-' . $m . '-' . $d == $dateferie && $w == 7) {
                                                        $couleur = 'table-dark text-danger';
                                                    } elseif ($year . '-' . $m . '-' . $d == $dateferie && $w == 6) {
                                                        $couleur = 'table-dark text-success';
                                                    }
                                                }
                                                echo $couleur;   ?>  text-center"><?= $date->days[$w - 1] . ' ' . $d . ' ' . $date->months[$m - 1] . ' ' . $year ?>
                            </td>
                        </tr>
                    <?php } ?>
                    <?php
                    if ((count($datesSeul[$m])) < 31) {
                        for ($i = 0; $i < 31 - count($datesSeul[$m]); $i++) {
                            echo '<tr><td id="td" class="table-secondary"></td></tr>';
                        }
                    }
                    ?>
                </table>
            </div>
        <?php } ?>
    </div>
</div>

<?php $content = ob_get_clean(); ?>
<?php require('views/template.php'); ?>