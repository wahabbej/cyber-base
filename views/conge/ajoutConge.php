<?php 
$title= 'accueil admin';
ob_start();
?>
<div class="text-center "><h1>Ajouter un Congé</h1></div>
<div class="col-4 offset-4" id="alert"></div>

<div class="container d-flex justify-content-center">
    <div class="col-md-12  col-sm-12 col-lg-6 shadow-lg p-5 bg-body rounded">
        <form novalidate action="index.php?path=conge&action=confirmAjout" method="POST">
                    <label for="libelle"  class="form-label">Libellé</label>
                    <input required id="libelle" placeholder="Libelle" class="form-control rounded-pill" type="text" name="libelle">
                    <label for="code"  class="form-label">Code</label>
                    <input required id="code" placeholder="Code congé" class="form-control rounded-pill" type="text" name="code">
                    <label for="codeCouleur"  class="form-label">Code couleur</label>
                    <input required id="codeCouleur" placeholder="code Couleur" class="form-control rounded-pill py-3" type="color" name="codeCouleur">
                    <label for="attribue"  class="form-label">Jours attribués </label>
                    <input required id="attribue" placeholder="Nombre de jour attribué" class="form-control rounded-pill " type="text" name="attribue">
                    <div class="row d-flex justify-content-between mt-4 ">
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                    <a class="btn btn-success rounded-circle" href="index.php?path=conge&action=listeConge"> <img class="text-success" src="src/images/arrow-left-circle-fill.svg" alt=""></a>
                </div>
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                    <input type="hidden" name="tokenT" value="<?=$token?>">
                    <button class="btn btn-warning" type="submit" ><img src="src/images/plus-square.svg" alt=""></button>
            </div>   
        </div>
        </form> 
    </div>
</div>
<?php $content= ob_get_clean();
require 'views/template.php';