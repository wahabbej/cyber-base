<?php 
$title= 'Liste des congés';
ob_start();
?>
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Liste des catégories de congés</h1>
    </div>
</div>
<?php if (!empty($_SESSION['success'])){?>
    <div class="alert alert-success col-md-4 offset-4" role="alert" id="alerte">
        <?php 
            echo $_SESSION["success"];
            unset($_SESSION["success"]);
        ?>
    </div>
<?php }?>
<div class="container pb-4 mb-5">
    <div class="d-flex justify-content-end mt-2"><a class="btn btn-primary rounded-circle px-4 py-3" href="index.php?path=conge&action=ajoutConge">+</a></div>
<table class="table ">
    <tr class="text-center">
        <th>Identifiant</th>
        <th>Libellé (description)</th>
        <th>Code </th>
        <th>Code Couleur</th>
        <th>Nonbre de jours Attribués</th>
        <th>Action</th>
    </tr>
    <?php foreach($resultatconges as $resultatconge){ ?>
    <tr class="text-center" style="background-color:<?= $resultatconge->getCodeCouleur()?> ;">
        <td class="py-4"><?= $resultatconge->getIdConge()?></td>
        <td><?= $resultatconge->getLibelle()?></td>
        <td><?= $resultatconge->getCode()?></td>
        <td><?= $resultatconge->getCodeCouleur()?></td>
        <td><?= $resultatconge->getJourAttribue()?></td>
        <td class="bg-white"><a class="btn btn-primary" href="index.php?path=conge&action=profileConge&id=<?= $resultatconge->getIdConge()?>">Editer</a></td>
    </tr>
    <?php }?>
</table>
</div>
<?php $content= ob_get_clean();
require 'views/template.php';