<?php 
$title='Modifier Congé';
ob_start();
?>
<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5"> Modifier congé</h1>
    </div>
</div> 
<div class="container d-flex justify-content-center">
    <div class="col-12 col-sm-12 col-md-12 col-lg-6 shadow-lg p-4 bg-body rounded">
            <form action="index.php?path=conge&action=confirmerModif" method="POST">
                    <label for="libelle"  class="form-label ">Libellé</label>
                    <input id="libelle" value="<?=$resultats->getLibelle()?>" class="form-control rounded-pill" type="text" name="libelle">
                    <label for="code"  class="form-label mt-4">Code</label>
                    <input id="code" value="<?=$resultats->getCode()?>" class="form-control rounded-pill" type="text" name="code">
                    <label for="codeCouleur"  class="form-label mt-4">Code couleur</label>
                    <input id="codeCouleur" value="<?=$resultats->getCodeCouleur()?>" class="form-control rounded-pill" type="color" name="codeCouleur">
                    <label for="jourAttribue"  class="form-label mt-4">Nombre de jours attribués</label>
                    <input id="jourAttribue" value="<?=$resultats->getJourAttribue()?>" class="form-control rounded-pill" type="number" name="jourAttribue">
                    <input value="<?=$resultats->getIdConge()?>" type="hidden" name="id">
                    <div class="d-flex justify-content-between mt-5">
                    <input type="hidden" name="tokenT" value="<?=$token?>">
                        <a class="btn btn-success" href="index.php?path=conge&action=profileConge&id=<?=$resultats->getIdConge()?>"><img class="text-success" src="src/images/arrow-left-circle-fill.svg" alt=""> </a>
                        <button class="btn btn-warning" type="submit" ><img  src="src/images/pencil-square.svg" alt=""></button>
                    </div>
                </div>   
            </div>
        </form> 
    </div>
</div>

<?php $content= ob_get_clean();
require('views/template.php');