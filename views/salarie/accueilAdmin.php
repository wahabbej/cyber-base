<?php 
$title= 'accueil admin';
ob_start();
?>
<div class="container d-flex justify-content-center mb-3 mt-3">
    <div class="text-center mt-4  shadow-sm p-3  bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Espace admin</h1>
    </div>
</div>

<div class="container d-flex justify-content-center mb-2">
    <div class="col-12 col-sm-12 col-md-12 col-lg-6 shadow-lg p-5  bg-body rounded">
        <div class="row  ">
            <div class="row d-flex justify-content-between">
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil ">
                <a  href="index.php?path=salarie&action=listeSalarie">Gérer les salariés</a>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil ">
                <a  href="index.php?path=salarie&action=gererCongeSalarie">Gérer les congés des salaries</a>
                </div>
            </div>
            <div class="row d-flex justify-content-between ">
                <div class="col-xl-6 col-md-6 col-sm-6 col-12  accueil">
                <a  href="index.php?path=salarie&action=gererHeureSupp">Gérer les Heures Supp des salaries</a>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil ">
                <a  href="index.php?path=salarie&action=compteEpargne">Editer le Compte epargne temps</a>
                </div>
            </div>
            <div class="row d-flex justify-content-between ">
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil">
                    <a href="index.php?path=salarie&action=gererHistotique" > Historique des congés </a>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil ">
                    <a  href="index.php?path=conge&action=listeConge">Catégorie congés</a>       
                </div>
            </div>
            <div class="row d-flex justify-content-between ">
                <div class="col-xl-6 col-md-6 col-sm-6 col-12  accueil">
                <a  href="index.php?path=salarie&action=planing">Planing des congés  </a>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil">
                <a  href="index.php?path=salarie&action=editeCongeManuel">Editer les congés manuellement </a>   
                </div>
            </div>
            <div class="row d-flex justify-content-between ">
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil">
                <a  href="index.php?path=salarie&action=listeCongeSoumis">congés en attente attente de validation </a> 
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil">
                <a  href="index.php?path=conge&action=calendrier">Calendrier</a>  
                </div>
            </div>
            <div class="row d-flex justify-content-end mt-5 ">
                <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 col-12 text-end">
                <a class="btn btn-danger pull-right "  href="index.php?path=salarie&action=deconnexion"><img src="src/images/box-arrow-right.svg" alt="">    Déconnexion </a> 
                </div>
               
            </div>
        </div>
    </div>
</div>

<?php $content= ob_get_clean();
require 'views/template.php';
