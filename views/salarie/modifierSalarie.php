<?php
$title = 'Modifier Salarié';
ob_start();
?>
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-3 mb-4 shadow-sm p-3 mb-3 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Modifier salarié  </h1>
    </div>
</div>
<div class="container">
    <?php if(isset($_SESSION['flash'])){
        foreach($_SESSION['flash'] as $class =>$message){
        ?>
        <div class="row d-flex justify-content-center">
            <div class="alert alert-<?=$class?> col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center" id="alerte" role="alert">
                <?php 
                echo $message;
                unset($_SESSION["flash"]);
                ?>
            </div>
        </div>
    <?php }}?>
</div>
<div class="container d-flex justify-content-center">
    <div class="col-md-12  col-sm-12 col-lg-6 shadow-lg p-5 bg-body rounded">
        <form action="index.php?path=salarie&action=confirmerModif" method="POST">
            <label for="nom" class="form-label ">Nom</label>
            <input id="nom" value="<?= $resultats->getNom() ?>" class="form-control rounded-pill" type="text" name="nom">
            <label for="prenom" class="form-label mt-3">Prenom</label>
            <input id="prenom" value="<?= $resultats->getPrenom() ?>" class="form-control rounded-pill" type="text" name="prenom">
            <label for="mail" class="form-label mt-3">Email</label>
            <input id="mail" value="<?= $resultats->getEmail() ?>" class="form-control rounded-pill" type="email" name="mail">
            <label for="tauxHoraire" class="form-label mt-3">Taux horaire</label>
            <input id="tauxHoraire" value="<?= $resultats->getTauxHoraire() ?>" class="form-control rounded-pill" type="text" name="tauxHoraire">                
            <label for="nbJourSemaine" class="form-label mt-3">Nombre jour semaine</label>
            <input id="nbJourSemaine" value="<?= $resultats->getNbJourSemaine() ?>" class="form-control rounded-pill" type="text" name="nbJourSemaine">
            <label for="rol" class="form-label mt-3">Fonction</label>
            <input type="hidden" name="tokenT" value="<?=$token?>">
            <select id="rol" name="rol" class="form-select rounded-pill">
            <?php foreach ($resulatRoles as $resulatRole) { ?>
                 <option <?php if ($resultats->getIdRole() == $resulatRole->getId()) 
                                    {
                                        echo 'selected';
                                    } ?> value="<?= $resulatRole->getId() ?>"> <?= $resulatRole->getNom() ?></option>
             <?php } ?>
            </select> 
            <input value="<?= $resultats->getIdSalarie() ?>" type="hidden" name="id">
            <div class="row d-flex justify-content-between mt-4 ">
                <div class="col-2 col-sm-2 col-md-2 col-lg-2 ">
                    <a class="btn btn-success" href="index.php?path=salarie&action=profileSalarie&id=<?= $resultats->getIdSalarie() ?>"><img class="text-success" src="src/images/arrow-left-circle-fill.svg" alt=""></a>
                </div>
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                    <button class="btn btn-warning" type="submit"><img  src="src/images/pencil-square.svg" alt=""></button>
                </div>
            </div>
        </form>
    </div>
</div>
<?php $content = ob_get_clean();
require('views/template.php');
