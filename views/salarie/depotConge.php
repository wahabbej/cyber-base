<?php
$title = 'Depot congé  ';
ob_start();
?>
<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Dépot conge  </h1>
    </div>
</div>
<div class="container">
    <?php if(isset($_SESSION['flash'])){
        foreach($_SESSION['flash'] as $class =>$message){
        ?>
        <div class="row d-flex justify-content-center">
            <div class="alert alert-<?=$class?> col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center"  role="alert">
                <?php 
                echo $message;
                unset($_SESSION["flash"]);
                ?>
            </div>
        </div>
    <?php }}?>
</div>
 
<div class="container d-flex justify-content-center">
    <div class="col-md-12  col-sm-12 col-lg-6 shadow-lg p-5 mb-5 bg-body rounded">
        <form novalidate class="form" action="index.php?path=congeAquis&action=depotConge" method="POST">
            <label class="form-label " for="dateDebut">choisir la date de debut </label>
            <input required class="form-control" type="date" id="dateDebut" name="dateDebut">
            <input type="hidden" name="tokenT" value="<?=$token?>">
            <label class="form-label mt-4" for="dateFin">choisir la date de fin </label>
            <input required class="form-control" type="date" id="dateFin" name="dateFin">
            <label class="form-label mt-4" for="categorie">catégorie de congé  </label>
            <select required name="idConge" class="form-select">
                <option value="" selected>Choisir la catégorie</option>
                <?php foreach($resultatConges as $resultatConge){ ?>
                <option value="<?= $resultatConge->getIdConge() ?>"><?= $resultatConge->getLibelle() ?></option>
                <?php }?>
            </select>
            <button class="btn btn-success mt-4" type="submit"><img src="src/images/plus-square.svg" alt=""></button>
        </form>
    </div>
</div>
<?php $content = ob_get_clean();
require 'views/template.php';