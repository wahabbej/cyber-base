<?php
$title = 'Espace salarié  ';
ob_start();
?>
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Espace Salarie</h1>
    </div>
</div>
<div class="text-center"><h3>Bienvenue <?=$resultatSalarie->getNom(). ' '.$resultatSalarie->getPrenom() ?> </h3></div>

<div class="container d-flex justify-content-center">
    <div class="col-12 col-sm-12 col-md-12 col-lg-6 shadow-lg p-5 mb-5 bg-body rounded">
        <div class="row mt-5 ">
            <div class="row d-flex justify-content-between">
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil ">
                <a  href="index.php?path=salarie&action=listeCongeSalarie">Consulter mes congés</a>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil ">
                <a  href="index.php?path=salarie&action=listeHeureSupp">consulter mes heures Supp</a>
                </div>
            </div>
            <div class="row d-flex justify-content-between ">
                <div class="col-xl-6 col-md-6 col-sm-6 col-12  accueil">
                <a  href="index.php?path=salarie&action=depotConge">Déposer un congé</a>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-6 col-12 accueil ">
                <a  href="index.php?path=salarie&action=planing">Planing des congés  </a>
                </div>
            </div>
            <div class="row d-flex justify-content-between ">
                <div class="col-xl-6 col-md-6 col-sm-6 col-6  accueil">
                <a href="index.php?path=conge&action=calendrier">Calendrier</a>
                </div>
            </div>
            <div class="row d-flex justify-content-end mt-5 ">
                <div class="col-lg-6 col-xl-6 col-md-6 col-sm-6 col-12 text-end">
                    <a class="btn btn-danger pull-right "  href="index.php?path=salarie&action=deconnexion"><img src="src/images/box-arrow-right.svg" alt="">    Déconnexion </a> 
                </div>
            </div>
        </div>
    </div>
</div>
<?php $content = ob_get_clean();
require 'views/template.php';