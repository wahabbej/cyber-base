<?php 
$title= 'Liste heures supp';
ob_start();
$jour = 0;
$somme= 0;
?>
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Liste des heures supp</h1>
    </div>
</div>

<div class="container mb-5">
    <div class="d-flex justify-content-end col-8 mt-2"></div>
    <div class="row">
        <div class="col-12 col-md-8 col-sm-12 col-lg-8 mx-0">
            <table class="table table-dark table-hover">
                <tr>
                    <th colspan="5" class="text-center"><?=$resultat->getNom().' '. $resultat->getPrenom() ?></th>
                </tr>

                <tr>
                    <th >Jour</th><th>Date</th><th>Heure début</th><th>Heure fin</th><th>Nombre d'heures</th>
                </tr>
                <?php foreach($resulatHeureSupps as $resulatHeureSupp) 
                {?>
                <tr class="<?php if($resulatHeureSupp->getDate()==null)
                                    {
                                        echo 'd-none';
                                    } ?>">
                    <td>jour <?= $jour=$jour+1?></td>
                    <td><?=transformDate($resulatHeureSupp->getDate())->format('d-m-Y') ?></td>
                    <td><?=transformDate($resulatHeureSupp->getHeureDebut())->format('H:i') ?></td>
                    <td><?=transformDate($resulatHeureSupp->getHeureFin())->format('H:i') ?></td>
                    <td><?= $resulatHeureSupp->getNbMinute()?> m</td>
                    <?php $somme+=$resulatHeureSupp->getNbMinute();?>
                </tr>
                <?php } ?> 
                        <?php $tSH = $resultat->getTauxHoraire()/$resultat->getNbJourSemaine() ?>   
            </table>
        </div>
        <div class=" col-lg-4 col-md-4 col-sm-12 col-12 mx-0 ">
                <table class="table table table-success table-hover text-center">
                    <tr><th>Total heures supp</th><th>Total jours supp</th><th>Base de calcule jour</th></tr>
                    <tr><td id="cumulHeures"><?= round(fmod($somme/60,$tSH),2) ?> h</td><td id=cumulJours><?= floor(($somme/60)/$tSH);?>  jour(s)</td><td><?=$tSH?> </td></tr>
                    
                </table>   
        </div>
    </div>
</div>


<?php 
$content=ob_get_clean();
require ('views/template.php');