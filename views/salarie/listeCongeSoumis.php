<?php
$title = 'Liste des congé soumis ';
ob_start(); ?>

<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Congés soumis à la validation</h1>
    </div>
</div>
<div class="container">
    <?php if(isset($_SESSION['flash'])){
        foreach($_SESSION['flash'] as $class =>$message){
        ?>
        <div class="row d-flex justify-content-center">
            <div class="alert alert-<?=$class?> col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center" id="alerte" role="alert">
                <?php 
                echo $message;
                unset($_SESSION["flash"]);
                ?>
            </div>
        </div>
    <?php }}?>
</div>
 
    <div class="table-liste">
        <table id="tableau" class="table table-striped text-center">
            <tr><th>Nom</th><th>Prenom</th><th>Date debut</th><th>Date Fin</th><th>Type de congé</th><th>Nombre de jours</th><th colspan="2">Action</th></tr>
            <?php foreach($resulatCongeAcquis as $resulatCongeAcqui) {?>
                
                <tr>
                    <td><?= $resulatCongeAcqui->nom ?></td>
                    <td><?= $resulatCongeAcqui->prenom ?></td>
                    <td><?=transformDate($resulatCongeAcqui->dateDebut)->format('d-m-Y')  ?></td>
                    <td><?=transformDate($resulatCongeAcqui->dateFin)->format('d-m-Y')  ?></td>
                    <td><?= $resulatCongeAcqui->code ?></td>
                    <td><?= $resulatCongeAcqui->acquis ?></td>
                    <td>
                        <a class="btn btn-danger" href="index.php?path=congeAquis&action=rejetConge&idCongeAcquis=<?=$resulatCongeAcqui->idCongeAcquis?>">Rejeter</a>
                    </td>
                    <td>
                        <form action="index.php?path=congeAquis&action=accepterConge" method="POST" >
                            <input type="hidden" name="dateDebut" value="<?=$resulatCongeAcqui->dateDebut?>">
                            <input type="hidden" name="dateFin" value="<?=$resulatCongeAcqui->dateFin?>">
                            <input type="hidden" name="idSalarie" value="<?=$resulatCongeAcqui->idSalarie?>">
                            <input type="hidden" name="idConge" value="<?=$resulatCongeAcqui->idConge?>" >
                            <input type="hidden" name="idCongeAcquis" value="<?=$resulatCongeAcqui->idCongeAcquis?>" >
                            <button class="btn btn-success"  type="submit">Valider</button>
                        </form>
                    </td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>

<?php

$content = ob_get_clean();
require('views/template.php');