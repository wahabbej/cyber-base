<?php 
$title='Ajout Salarié';
ob_start();
?>

<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3  bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Ajouter un salarie  </h1>
    </div>
</div>
<div class="col-4 offset-4" id="alert"></div>
<div class="container">
    <?php if(isset($_SESSION['flash'])){
        foreach($_SESSION['flash'] as $class =>$message){
        ?>
        <div class="row d-flex justify-content-center">
            <div class="alert alert-<?=$class?> col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 " role="alert">
                <?php 
                echo $message;
                unset($_SESSION["flash"]);
                ?>
            </div>
        </div>
    <?php }}?>
</div>
<div class="container d-flex justify-content-center">
    <div class="col-md-12  col-sm-12 col-lg-6 shadow-lg p-5 bg-body rounded">
        <form novalidate action="index.php?path=salarie&action=confirmAjout" method="POST">
            <label for="nom"  class="form-label">Nom</label>
            <input required id="nom" placeholder="Nom" class="form-control rounded-pill" type="text" name="nom">
            <label for="prenom"  class="form-label mt-3">Prenom</label>
            <input required id="prenom" placeholder="prenom" class="form-control rounded-pill" type="text" name="prenom">
            <label for="mail"  class="form-label mt-3">Email</label>
            <input required id="mail" placeholder="Email" class="form-control rounded-pill" type="email" name="mail">
            <label for="rol"  class="form-label mt-3">Fonction</label>
            <select required id="rol"  name="rol" class="form-select rounded-pill" >
            <option selected>sélectionnez une categorie</option>
                <?php foreach($resulatRoles as $resulatRole){ ?>
                    <option  value="<?=$resulatRole->getId()?>"> <?=$resulatRole->getNom()?></option>
                <?php }?>
            </select>
            <input type="hidden" name="tokenT" value="<?=$token?>">
            <label for="mdp"  class="form-label mt-3">Mot de passe</label>
            <input required minlength="8" id="mdp" placeholder="Mot de passe" class="form-control rounded-pill" type="password" name="mdp">
            <label for="tauxhoraire"  class="form-label mt-4">Taux horaire </label>
            <input required  id="tauxhoraire" placeholder="Taux horaire" class="form-control rounded-pill" type="text" name="tauxhoraire">
            <label for="nbJourSemaine"  class="form-label mt-3">Nombre jour par semaine</label>
            <input required  id="nbJourSemaine" placeholder="Nombre de jour par semaine" class="form-control rounded-pill" type="text" name="nbJourSemaine">       
            <div class="row d-flex justify-content-between mt-4 ">
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                    <a class="btn btn-success" href="index.php?path=salarie&action=listeSalarie"> <img class="text-success" src="src/images/arrow-left-circle-fill.svg" alt=""></a>
                </div>
                <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                <button class="btn btn-success" type="submit" ><img src="src/images/plus-square.svg" alt=""></button>
                </div>   
            </div>
        </form> 
    </div>
</div>

<?php $content= ob_get_clean();
require('views/template.php');