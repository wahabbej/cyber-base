<?php
$title = 'Home';
ob_start() ;
?>
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Cyber-Base</h1>
    </div>
</div>
<div class="container">
    <?php if(isset($_SESSION['flash'])){
        foreach($_SESSION['flash'] as $class =>$message){
        ?>
        <div class="row d-flex justify-content-center">
            <div class="alert alert-<?=$class?> col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 " role="alert">
                <?php 
                echo $message;
                unset($_SESSION["flash"]);
                ?>
            </div>
        </div>
    <?php }}?>
</div>
<div class="container">
<div class="container d-flex justify-content-center">
    <div class="col-12 col-sm-12 col-md-12 col-lg-6 shadow-lg p-5 mb-5 bg-body rounded">
        <h1>Connexion</h1>
        <form  action="index.php?path=salarie&action=connexion" method="POST">
            <div id="alert"></div>
            <label for="inputEmail">Login* </label>
            <input name="login" id="inputEmail" type="email" required class="form-control">
            <label for="inputMdp">MDP* </label>
            <input name="mdp" id="inputMdp" type="password" required minlength="5" class="form-control">
            <button class="btn btn-danger mt-3">Connexion</button>
        </form>
    </div>
</div>

<?php $content = ob_get_clean(); ?>
<?php require('views/template.php'); ?>
