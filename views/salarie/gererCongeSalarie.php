<?php
$title = 'Liste des slariés';
ob_start(); ?>
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Gérer les congés</h1>
    </div>
</div>
<div class="container liste-slarie">

    <?php if (!empty($_SESSION['success'])) { ?>
        <div class="alert alert-success col-md-4 offset-4 liste-alert" role="alert" id="alerte"><?php
            echo $_SESSION["success"];
            unset($_SESSION["success"]);
            ?>
        </div>
    <?php } ?>
    <div class="d-flex justify-content-between mt-2 mb-4">
        <?php if(date('m')==6){ ?>
            <a class="btn btn-danger" href="index.php?path=salarie&action=miseNiveau">mise à jour anuelle</a>
        <?php }?>
        <?php if(date('m')==5){ ?>
            <a class="btn btn-danger" href="index.php?path=salarie&action=miseNiveauRelequat">mise à jour RELEQUAT</a>
        <?php }?>
    </div>
    <div class=" d-flex justify-content-end mt-2 mb-4">
        <form action="" method="POST" class="d-flex text-end col-12 col-sm-12 col-md-6 col-lg-4">
            <input id="recherche" class="form-control me-2" type="search" placeholder="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
    </div>
    <div class="table-liste">
        <table id="tableau" class="table table-striped ">
        </table>
    </div>
</div>
<script src="./src/js/CongeSalarie.js"></script>
<?php

$content = ob_get_clean();
require('views/template.php');
