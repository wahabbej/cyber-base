<?php
$title= 'Profile salarié';
ob_start();
?>
<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-3 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Profil salarié </h1>
    </div>
</div>
<div class="container">
    <?php if(isset($_SESSION['flash'])){
        foreach($_SESSION['flash'] as $class =>$message){
        ?>
        <div class="row d-flex justify-content-center">
            <div class="alert alert-<?=$class?> col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center " id="alerte" role="alert">
                <?php 
                echo $message;
                unset($_SESSION["flash"]);
                ?>
            </div>
        </div>
    <?php }}?>
</div>
    <div class="container d-flex justify-content-center">
    <div class="col-12 col-sm-12 col-md-12  col-lg-6 shadow-lg p-5 mb-5 bg-body rounded">
    <div class="row d-flex justify-content-between ">
        
        <div class="col-6 col-sm-6 col-md-6 col-lg-6  ">
            <h5>Nom :</h5>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5><?= $resultats->getNom()?></h5>
        </div>
    </div>
    <div class="row d-flex justify-content-between mt-4">
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5>Prenom :</h5>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5><?= $resultats->getPrenom()?></h5>
        </div>
    </div>
    <div class="row d-flex justify-content-between mt-4">
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5>email :</h5>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5><?= $resultats->getEmail()?></h5>
        </div>
    </div>
    <div class="row d-flex justify-content-between mt-4 ">
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5>Taux horaire :</h5>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5><?= $resultats->getTauxHoraire()?> h</h5>
        </div>
    </div>
    <div class="row d-flex justify-content-between mt-4">
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5>Nombre jour semaine :</h5>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5><?= $resultats->getNbJourSemaine()?> jours</h5>
        </div>
    </div>

    <div class="row d-flex justify-content-between mt-4">
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5>Fonction :</h5>
        </div>
        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
            <h5><?php foreach($resultatRoles as $resultatRole){if($resultatRole->getId() == $resultats->getIdRole()){ echo $resultatRole->getNom();}} ?></h5>
        </div>
    </div>
    <div class="row d-flex justify-content-between mt-4 ">
        <div class="col-2 col-sm-2 col-md-2 col-lg-2">
            <a class="btn btn-success " href="index.php?path=salarie&action=listeSalarie"><img class="text-success" src="src/images/arrow-left-circle-fill.svg" alt=""></a>
        </div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2  ">
        <a class="btn btn-warning  " href="index.php?path=salarie&action=modifierSalarie&id=<?=$resultats->getIdSalarie()?>"><img src="src/images/pencil-square.svg" alt=""></a>
        </div>
        <div class="col-2 col-sm-2 col-md-2 col-lg-2">
            <form action="index.php?path=salarie&action=supprimerSalarie" method="POST">
                <input type="hidden" value="<?= $resultats->getIdSalarie()?>" name="idSalarie">
                <input type="hidden" name="tokenT" value="<?=$token?>">
                <button type="submit" class="btn btn-danger " ><img  src="src/images/trash3.svg" alt=""></button>
            </form>
        
        </div>
    </div>
</div>
</div>


<?php $content= ob_get_clean();
require('views/template.php');