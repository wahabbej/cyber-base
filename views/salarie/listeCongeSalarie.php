<?php
$title = 'Liste des congés salariés';
ob_start(); 
$conge = 0 ?>

<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Liste des congés</h1>
    </div>
</div>

<div class="container">
    <table class="table table-dark ">
        <tr><th>congé</th><th>Date début</th><th>Date fin</th><th>Nombre de jours </th><th>Statut</th></tr>
        <?php foreach($resulatCongeAcquis as $resulatCongeAcqui) {?>
            <tr>
                <td><?=$conge=$conge+1?></td>
                <td><?= transformDate($resulatCongeAcqui->getDateDebut())->format('d/m/Y') ?></td>
                <td><?= transformDate($resulatCongeAcqui->getdateFin())->format('d/m/Y')?></td>
                <td><?=$resulatCongeAcqui->getAcquis()?> </td>
                <?php if($resulatCongeAcqui->getStatut()==1){?>
                <td class="table-success"><?='Validé'?></td>
                <?php }else{?>
                <td class="table-danger"><?='En attente'?></td>
                <?php }?>
            </tr>
        <?php }?>
    </table>
</div>

   
    
<?php
$content = ob_get_clean();
require('views/template.php');
