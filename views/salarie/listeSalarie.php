<?php
$title = 'Liste des salariés';
ob_start(); ?>
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Liste des salariés</h1>
    </div>
</div>
<div class="container">
    <?php if(isset($_SESSION['flash'])){
        foreach($_SESSION['flash'] as $class =>$message){
        ?>
        <div class="row d-flex justify-content-center">
            <div class="alert alert-<?=$class?> col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center " id="alerte" role="alert">
                <?php 
                echo $message;
                unset($_SESSION["flash"]);
                ?>
            </div>
        </div>
    <?php }}?>
</div>
<div class="container liste-slarie">
    <div class="d-flex justify-content-end mt-2 mb-2">
        <a class="btn btn-primary rounded-circle px-4 py-3" href="index.php?path=salarie&action=ajoutSalarie">+</a>
    </div>
    <div class=" d-flex justify-content-end mt-2 mb-4">
        <form action="" method="POST"  class="d-flex text-end col-12 col-sm-12 col-md-6 col-lg-4">
            <input id="recherche" class="form-control me-2" type="search" placeholder="Rescherche">
            <button class="btn btn-outline-success" type="submit">Recherche</button>
        </form>
    </div>
    <div class="table-liste">
        <table id="tableau" class="table table-striped ">
        </table>
    </div>
</div>
<script src="./src/js/liste.js"></script>
<?php

$content = ob_get_clean();
require('views/template.php');
