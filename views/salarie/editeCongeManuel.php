<?php
$title = 'editer les congé manuellement ';
ob_start(); ?>
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1>Editer les congé manuellement</h1>
    </div>
</div>
<div class="container liste-slarie ">
    <div class="d-flex justify-content-end mt-2 mb-2 "><a class="btn btn-primary rounded-circle px-4 py-3" href="index.php?path=salarie&action=ajoutSalarie">+</a></div>
    <div class="col-md-4 offset-8 mb-3">
        <form action="" class="d-flex text-end">
            <input id="recherche" class="form-control me-2" type="search" placeholder="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
    </div>
    <div class="table-liste">
        <table id="tableau" class="table table-striped ">
        </table>
    </div>
</div>
<script src="./src/js/editeCongeManuel.js"></script>
<?php
$content = ob_get_clean();
require('views/template.php');
