<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="src/css/bootstrap.css">
    <link rel="stylesheet" href="src/css/style.css">
    
    <title><?= $title?></title>
</head>
<body>
    <?php include 'views/fragments/menu.php'?>
    
    <?= $content?>

    <?php include 'views/fragments/footer.php'?>
    <script src="./src/js/bootstrap.js"></script>
    <script src="./src/js/alertSuccess.js"></script>
    <script src="./src/js/Cyber.js"></script> 
</body>
</html>