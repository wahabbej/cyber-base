<?php
$title= 'Modifier Heure supp';
ob_start();
?>

<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5"> Modifier heure supp</h1>
    </div>
</div>
<div class="container">
    <?php if(isset($_SESSION['flash'])){
        foreach($_SESSION['flash'] as $class =>$message){
        ?>
        <div class="row d-flex justify-content-center">
            <div class="alert alert-<?=$class?> col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center " id="alerte" role="alert">
                <?php 
                echo $message;
                unset($_SESSION["flash"]);
                ?>
            </div>
        </div>
    <?php }}?>
</div>
<div id="ajoutConge"  class="container d-flex justify-content-center">
<div class="col-md-12  col-sm-12 col-lg-6 shadow-lg p-5 mb-5 bg-body rounded">
    <form action="index.php?path=heureSupp&action=confirmModifHeureSupp" method="POST">
         <label for="dateDebut" class="form-label mt-4">Heure début</label>
            <div><input id="dateDebut" type="datetime-local" class="form-control " name="heureDebut" value="<?=date('Y-m-d\TH:i:s', strtotime($resultatHeureSupp->getDate().$resultatHeureSupp->getHeureDebut()))?>" ></div>
            <label for="datefin" class="form-label mt-3">Heure fin</label>
            <div><input id="dateFin" type="datetime-local" class="form-control" name="heureFin" value="<?=date('Y-m-d\TH:i:s', strtotime($resultatHeureSupp->getDate().$resultatHeureSupp->getHeureFin()))?>"></div>
            <input id="decomptet" type="hidden"  name="decomptet" value="">
            <input type="hidden" name="tokenT" value="<?=$token?>">
            <input name="idSalarie" value="<?=$resultatHeureSupp->getIdSalarie()?>" type="hidden">
            <div class="mt-3">Décompte jourr</div>
            <div class="" id="decompte"><?=$resultatHeureSupp->getNbMinute()?></div>
            <input name="decomptet0" value="<?=$resultatHeureSupp->getNbMinute()?>" type="hidden">
            <input type="hidden" name="idHeureSupp" value="<?=$resultatHeureSupp->getIdHeureSupp()?>">
            <div class="d-flex justify-content-between">
               <a class="btn btn-success mt-3 " href="index.php?path=heureSupp&action=profileHeureSupp&id=<?=$id?>&idSalarie=<?=$resultatHeureSupp->getIdSalarie()?>"><img class="text-success" src="src/images/arrow-left-circle-fill.svg" alt=""></a> 
               <button class="btn btn-warning mt-3" type="submit"><img  src="src/images/pencil-square.svg" alt=""></button>
                
            </div>
    </form>
</div>
</div> 
<script  src="./src/js/heureSupp.js"></script>
<?php $content= ob_get_clean();
require('views/template.php');