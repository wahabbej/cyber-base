<?php
$title= 'detaile heure supp';
ob_start();
?> 
<div class="container d-flex justify-content-center mt-5">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h1 class="px-5">Datail heure supp </h1>
    </div>
</div> 
<div class="container">
    <?php if(isset($_SESSION['flash'])){
        foreach($_SESSION['flash'] as $class =>$message){
        ?>
        <div class="row d-flex justify-content-center">
            <div class="alert alert-<?=$class?> col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center " id="alerte" role="alert">
                <?php 
                echo $message;
                unset($_SESSION["flash"]);
                ?>
            </div>
        </div>
    <?php }}?>
</div>
<div class="container d-flex justify-content-center">
    <div class="col-12 col-sm-12 col-md-12 col-lg-6 shadow-lg p-5 mb-5 bg-body rounded">
        <div class="row d-flex justify-content-beteween  "> 
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 ">
                <h5>Salarie :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 ">
                <h5><?php if($resultatSalarie->getIdSalarie()==$resultats->getIdSalarie()){echo $resultatSalarie->getNom();} ?></h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween mt-4 ">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Date :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6 ">
                <h5><?= transformDate($resultats->getDate())->format('d-m-Y') ?></h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween mt-4 ">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Heure debut :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5><?= transformDate($resultats->getHeureDebut())->format('H:i') ?></h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween mt-4 ">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>Heure fin :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5><?= transformDate($resultats->getHeureFin())->format('H:i') ?></h5>
            </div>
        </div>
        <div class="row d-flex justify-content-beteween mt-4 ">
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5>durée :</h5>
            </div>
            <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                <h5><?=  $resultats->getNbMinute() ?> m</h5>
            </div>
        </div>
        <div class="row d-flex justify-content-between mt-5 ">
            <div class="col-2 col-sm-2 col-md-2 col-lg-2 ">
                <a class="btn btn-success "href="index.php?path=salarie&action=editerheureSupp&id=<?=$resultats->getIdSalarie()?>" ><img class="text-success" src="src/images/arrow-left-circle-fill.svg" alt=""></a>  
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-2 ">
                <a class="btn btn-warning" href="index.php?path=heureSupp&action=modifHeureSupp&id=<?=$resultats->getIdHeureSupp()?>"><img  src="src/images/pencil-square.svg" alt=""></a>
            </div>
            <div class="col-2 col-sm-2 col-md-2 col-lg-2">
                <form action="index.php?path=heureSupp&action=supprimerHeureSupp&id=<?= $resultats->getIdHeureSupp()?>&idSalarie=<?= $resultats->getIdSalarie()?>" method="POST">
                <input type="hidden" name="tokenT" value="<?=$token?>">
                <button class="btn btn-danger " type="submit"><img  src="src/images/trash3.svg" alt=""></button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $content= ob_get_clean();
require('views/template.php');