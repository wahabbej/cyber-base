<?php 
$title= 'Editer heures supp';
ob_start();
$jour = 0;

?>
<div class="container d-flex justify-content-center mt-3">
    <div class="text-center mt-4 mb-4 shadow-sm p-3 mb-5 bg-body rounded border-5 border-warning border-end border-bottom">
        <h2>Editer les heures supplimentaires</h2>
    </div>
</div>
<div class="container">
    <?php if(isset($_SESSION['flash'])){
        foreach($_SESSION['flash'] as $class =>$message){
        ?>
        <div class="row d-flex justify-content-center">
            <div class="alert alert-<?=$class?> col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6 text-center " id="alerte" role="alert">
                <?php 
                echo $message;
                unset($_SESSION["flash"]);
                ?>
            </div>
        </div>
    <?php }}?>
</div>
<?php $tSH = $resultat->getTauxHoraire()/$resultat->getNbJourSemaine() ?>
<div class="container mb-5">
    <div class="row">
    <div class="col-12 col-sm-12 col-md-6 col-lg-6  ">
                <table class="table table-striped table-dark table-hover table-bordered text-center table-heaurSupp">
                    <tr><th>Total heures supp</th><th>Total jours supp</th><th>Base de calcule jour</th></tr>
                    <tr><td id="cumulHeures"><?= round(fmod($somme/60,$tSH),2) ?></td><td id=cumulJours><?= floor(($somme/60)/$tSH);?></td><td><?=$tSH?> </td></tr>
                    <form  novalidate action="index.php?path=heureSupp&action=ajoutJour" method="POST">
                        <tr >
                            <th colspan="3"> Ajouter des jours RECUP</th>
                        </tr>
                        <tr>
                            <td>
                                <input type="hidden" name="idSalarie" value="<?=$resultat->getIdSalarie() ?>">
                                <input required type="number" name="jours" >
                                
                            </td>
                            <td colspan="2">
                                <button class="btn btn-success" type="submit" >Ajouter</button>
                            </td>
                        </tr>
                    </form>
                </table>
            </div>
        <div id="ajoutConge"  class="col-12 col-sm-12 col-md-6  col-lg-6 shadow-lg p-2 mb-5 bg-body rounded d-non">
        
                        <div class="text-center"><h5>Nouvelles heures supp</h5></div>  
                        <form novalidate action="index.php?path=heureSupp&action=ajoutHeureSupp" method="POST">
                        <label for="dateDebut" class="form-label mt-2">Heure début</label>
                        <input required id="dateDebut" type="datetime-local" class="form-control " name="dateDebut">
                        <label for="dateFin" class="form-label mt-4"> Heure fin</label>
                        <input required id="dateFin" type="datetime-local" class="form-control " name="dateFin">
                        <input type="hidden" name="tokenT" value="<?=$token?>">
                        <input id="decomptet" type="hidden"  name="decomptet" value="">
                        <input name="idSalarie" value="<?=$resultat->getIdSalarie()?>" type="hidden">
                        <div class="text-center form-control mt-4" id="decompte"></div>
                        <input type="hidden" name="idConge" value="6">
                       
                        
                            <button class="btn btn-success mt-4"  type="submit"><small>soumettre</small>  </button>
                      
                
        </div>
        
    </div>

    <div class="row">
        <div class="col-12 col-md-12 col-sm-12 col-lg-12 mx-0">
            <table class="table table-striped table-hover table-bordered table-heureSupp2">
                <tr>
                    <th colspan="6" class="text-center"><?=$resultat->getNom().' '. $resultat->getPrenom() ?></th>
                    
                </tr>

                <tr>
                    <th >Jour</th><th>Date</th><th>Heure début</th><th>Heure fin</th><th>Nombre d'heures travaillées</th><th>Action</th>
                </tr>
                 <?php foreach($resulatHeureSupps as $resulatHeureSupp) {?>
                <tr class="<?php if($resulatHeureSupp->getDate()==null){
                                echo 'd-none';
                            } ?>">
                                <td>jour <?= $jour=$jour+1?></td>
                                <td><?= transformDate($resulatHeureSupp->getDate())->format('d-m-Y') ?></td>
                                <td><?= transformDate($resulatHeureSupp-> getHeureDebut())->format('H:i') ?></td>
                                <td><?= transformDate($resulatHeureSupp-> getHeureFin())->format('H:i') ?></td>
                                <td><?= $resulatHeureSupp->getNbMinute()?> m</td>
                                <td><a class="btn btn-success " href="index.php?path=heureSupp&action=profileHeureSupp&id=<?=$resulatHeureSupp->getIdHeureSupp()?>&idSalarie=<?=$resultat->getIdSalarie()?>">Editer</a></td>
                            <?php } ?> 
                        <input type="hidden" id="cumulJoursT" name="cumulJours" value="">
                        
                        <input  id="somme" type="hidden" value="<?=$somme?>" >
                        <input id="tsh" type="hidden" value="<?=$tSH?>" >
                    </form>
                </tr> 
            </table>
        </div>
        
    </div>
</div>


<script src="./src/js/heureSupp.js"></script>
<?php 
$content=ob_get_clean();
require ('views/template.php');