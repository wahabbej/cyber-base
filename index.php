<?php
session_start();


spl_autoload_register(function ($class_name) {
    if (strstr($class_name, "Manager")) {
        include "models/managers/" . $class_name . ".class.php";
    } else {
        include "models/classes/" . $class_name . ".class.php";
    }
});

require 'models/config.php';
if (empty($_SESSION['token'])) {
    $_SESSION['token'] = bin2hex(random_bytes(32));
}
$token = $_SESSION['token'];


$year =  date('Y');

if (file_exists('src/document/jourFerie' . $year . '.txt') == false) {
    $donnees = file_get_contents('https://calendrier.api.gouv.fr/jours-feries/metropole/' . $year . '.json');
    $dossierJourF = 'src/document/jourFerie' . $year . '.txt';
    file_put_contents($dossierJourF, $donnees);
}

$path = 'main';
if (isset($_GET['path'])) {
    $path = securiser($_GET['path']);
}
$objCongeAquis = new CongeAcquisManager();
$resultatCongeAcquis= $objCongeAquis->getInvalideConge();
$nbLigne = count($resultatCongeAcquis);
                
switch ($path) {
    case "main":
        include("controllers/Controleur.php");
        break;
    case 'salarie':
        include("controllers/ControleurSalarie.php");
        break;
    case 'conge':
        include("controllers/ControleurConge.php");
        break;
    case 'congeAquis':
        include("controllers/ControleurCongeAcquis.php");
        break;
    case 'heureSupp':
        include("controllers/ControleurHeureSupp.php");
        break;
    case 'cumuleConge':
        include("controllers/ControleurCumuleConge.php");
        break;
}
