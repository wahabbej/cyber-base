
<?php

if (isset($_GET['action']))
{
    $action = securiser($_GET['action']);
}


switch ($action)
{
    case 'connexion':
        if (isset($_POST["login"])&&isset($_POST["mdp"])&& !empty($_POST["login"])&& !empty($_POST["mdp"]))
        {
            $login = securiser($_POST["login"]);
            $mdp= securiser($_POST["mdp"]);
            $objSalarie= new SalarieManager();
            $reps = $objSalarie->verifier($login);
            
            if (count($reps)>0  )
            {
                foreach ($reps as $rep){

                    if($rep->getIdRole()==1 && password_verify( $mdp, $rep->getMdp()))
                    {
                        $_SESSION['autorisation']='admin';
                        header('location:index.php?path=salarie&action=accueiladmin');
                    }
                    
                    elseif($rep->getIdRole()==2 && password_verify($mdp,$rep->getMdp())) 
                    {
                        $_SESSION['autorisation']='salarie';
                        $_SESSION['idSalarie']= $rep->getIdSalarie();
                        header('location:index.php?path=salarie&action=accueilSalarie');
                    }else
                    {
                        $_SESSION['flash']['danger']='echec de connexion';
                        include 'views/salarie/connexion.php';
                    }
                }
            }else{
                $_SESSION['flash']['danger']='echec de connexion';
                        include 'views/salarie/connexion.php';
            }
        }
         break;

    
    case 'deconnexion';
        
    if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin'||$_SESSION['autorisation']=='salarie')
                {
                
                $objSalarie= new SalarieManager();
                $objSalarie->deconnecter();
                header ('location:index.php?path=main&action=home');
                
            }}else{
            include('views/salarie/connexion.php');
        }
        break;

    case 'accueiladmin':
        
        if (isset($_SESSION['autorisation']))
        {
                if( $_SESSION['autorisation']=='admin')
                {
                    include('views/salarie/accueilAdmin.php');
                }
        }else{
            include('views/salarie/connexion.php');
        }
        break;
    
    case 'accueilSalarie':
        if(isset($_SESSION['autorisation']))
        {
            $objSalarie = new SalarieManager();
            $resultatSalarie = $objSalarie->getElementById($_SESSION['idSalarie']);
            $_SESSION['idSalarie'];
            include('views/salarie/accueilSalarie.php');
        }break;


    case 'listeSalarie':
        
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
            {
                $objSalarie=new SalarieManager();
                $resulats=$objSalarie->afficherTout();
                include('views/salarie/listeSalarie.php');
            }
        }else
        {
            include('views/salarie/connexion.php');
        }
        break;

    case 'gererCongeSalarie':
        
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    include('views/salarie/gererCongeSalarie.php');
                }
        }else
        {
            include('views/salarie/connexion.php');
        }
        break;

    case 'gererHeureSupp':
        
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
            {    
                include('views/salarie/gererHeureSupp.php');
            }
        }else
        {
            include('views/salarie/connexion.php');
        }
        break;

    case 'editeCongeManuel':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    include('views/salarie/editeCongeManuel.php');
                }    
        }else{
            include('views/salarie/connexion.php');
        }
        break;

    case 'compteEpargne':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
            {
                include('views/salarie/compteEpargne.php');
            }
        }else
        {
            include('views/salarie/connexion.php');
        }
        break;

    case 'profileSalarie':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
            {
                $id =securiser($_GET['id']);
                $objSalarie= new SalarieManager();
                $resultats=$objSalarie->getElementById($id);
                if (!$resultats){
                    $_SESSION['flash']['dager']= 'Le salarie n\'existe pas ';
                    header('location:index.php?path=salarie&action=listeSalarie');
                }
                $objconge= new RoleManager();
                $resultatRoles= $objconge->afficherTout();
                include('views/salarie/profileSalarie.php');
            }
        }else
        {
            include('views/salarie/connexion.php');
        } 
        break;

       
    case 'modifierSalarie':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
            { 
                $id =securiser($_GET['id']);
                $objSalarie= new SalarieManager();
                $resultats=$objSalarie->getElementById($id);
                $objRole= new RoleManager();
                $resulatRoles= $objRole->afficherTout();
                include ('views/salarie/modifierSalarie.php');  
            }
        }else
        {
            include('views/salarie/connexion.php');
        }  
        break;


    case 'confirmerModif':
        if (isset($_SESSION['autorisation']))
        {
            $tokenT = $_POST['tokenT'];
            if( $_SESSION['autorisation']=='admin' && $tokenT==$token)
            {
                $id = securiser($_POST['id']);
               
                if (!empty($_POST['nom'])&&!empty($_POST['prenom'])&&!empty($_POST['mail'])&&!empty($_POST['tauxHoraire'])&&!empty($_POST['nbJourSemaine'])&&!empty($_POST['rol']))
                {
                    
                    $prenom= securiser($_POST['prenom']);
                    $nom= securiser($_POST['nom']);
                    $mail= securiser($_POST['mail']);
                    $tauxHoraire = securiser($_POST['tauxHoraire']);
                    $nbJourSemaine = securiser($_POST['nbJourSemaine']);
                    $idRole= securiser($_POST['rol']);
                    $salarie= new Salarie();
                    $salarie->hydrate($nom,$prenom,$mail,$tauxHoraire,$nbJourSemaine,$idRole);
                    $salarie->setIdSalarie($id);
                    $objSalarie= new SalarieManager();
                    $resultat=$objSalarie->modifierSalarie($salarie);
                    if ($resultat)
                    {
                        $_SESSION['flash']['success']='Les modification ont bien été prises en compte';
                        header("location:index.php?path=salarie&action=profileSalarie&id=$id");
                    } 
                }else{
                    $_SESSION['flash']['danger'] = 'tous les champs sont requis';
                    header("location:index.php?path=salarie&action=modifierSalarie&id=$id");
                }
            }
        }else
        {
            include('views/salarie/connexion.php');
        }    
        break;


    case 'ajoutSalarie':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
            {
                $objRole= new RoleManager();
                $resulatRoles= $objRole->afficherTout();
                include('views/salarie/ajoutSalarie.php');
            }
        }else
        {
            include('views/salarie/connexion.php');
        }   
        break;


    case 'confirmAjout':
        if (isset($_SESSION['autorisation']))
        {
            $tokenT = $_POST['tokenT'];
           
            if( $_SESSION['autorisation']=='admin'&&$tokenT==$token)
            {
                if (!empty($_POST['nom'])&&!empty($_POST['prenom'])&& !empty($_POST['mail'])&&!empty($_POST['mdp'])&&!empty($_POST['tauxhoraire'])&&!empty($_POST['nbJourSemaine'])&&!empty($_POST['rol']))
                {
                    $nom= securiser($_POST['nom']);
                    $prenom= securiser($_POST['prenom']);
                    $email= securiser($_POST['mail']);
                    $objSalarie= new SalarieManager();
                    $reps = $objSalarie->verifier($email);
                
                    if (count($reps)>0  ){
                        $_SESSION['flash']['danger'] = 'ce salarie existe déja, veillez saisir un email valide';
                        header('location:index.php?path=salarie&action=ajoutSalarie');
                    }else{
                        $mdp= password_hash(securiser($_POST['mdp']),PASSWORD_DEFAULT,['cost'=>12]) ;
                        $idRole= securiser($_POST['rol']);
                        $tauxhoraire = securiser($_POST['tauxhoraire']);
                        $nbJourSemaine = securiser($_POST['nbJourSemaine']);
                        
                        $salarie= new Salarie();
                
                        $salarie->hydrate($nom,$prenom,$email,$tauxhoraire,$nbJourSemaine,$idRole);
                        $salarie->setMdp($mdp);
                        $resultat=$objSalarie->ajoutSalarie($salarie);
                        $resultatVerifs = $objSalarie->verifier($email);
                        $salarieConge = new SalarieConge();
                        $objSalarieConge = new SalarieCongeManager();
                        $objconge = new CongeManager();
                        $resultatConges = $objconge->afficherTout();
                        foreach ($resultatConges as $resultatConge)
                        {
                            foreach($resultatVerifs as $resultatVerif )
                            {
                                $salarieConge->hydrate($resultatVerif->getIdSalarie(),$resultatConge->getIdConge(),$resultatConge->getJourAttribue());
                                $objSalarieConge->ajoutCumuleJour($salarieConge);
                            }
                        }    
                        $_SESSION['flash']['success']= 'Le salarie '. $nom.'a bien été ajouté';
                        header('location:index.php?path=salarie&action=listeSalarie');
                        
                    }
            }else{
                $_SESSION['flash']['danger']='Tous les champs sont requis ';
            }            
        }
        }else
        {
            include('views/salarie/connexion.php');
        }     
        break;


    case 'supprimerSalarie':
        if (isset($_SESSION['autorisation']))
        {
            $tokenT = $_POST['tokenT'];
            if( $_SESSION['autorisation']=='admin' && $tokenT==$token)
            {
                $idSalarie=securiser($_POST['idSalarie']);
                $objSalarie= new SalarieManager();
                $resultatSalarie = $objSalarie->getElementById($idSalarie);
                if($resultatSalarie)
                {
                   $resultat=$objSalarie->supprimerSalarie($idSalarie);
                if ($resultat)
                {
                    $_SESSION['flash']['success']= 'Le salarie a bien été supprimé ';
                    header('location:index.php?path=salarie&action=listeSalarie');
                } 
                }else{
                    $_SESSION['flash']['dager']= 'Le salarie n\'existe pas ';
                    header('location:index.php?path=salarie&action=listeSalarie');
                }
                
            }
        }else
        {
            include('views/salarie/connexion.php');
        }
        break;


    case 'editerconge':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
            {
                $id=securiser($_GET['id']);
                $objcongeAcquis= new CongeAcquisManager();
                $resultats=$objcongeAcquis->afficherCongeJoinPris($id);
                $objSalarieConge = new SalarieCongeManager();
                $resultatSalarieConges = $objSalarieConge->getElementByIdSalarie($id);
                $objSalarie=new SalarieManager();
                $resultat=$objSalarie->getElementById($id);
                $objConge= new CongeManager();
                $resultatConges= $objConge->afficherTout();
                $objcongeAcquis= new CongeAcquisManager();
                $resulatCongeAcquis= $objcongeAcquis->getElementByIdSalarie($id);  
                include('views/congeAcquis/editerCongeAcquis.php');  
            }      
        }else
        {
            include('views/salarie/connexion.php');
        }   
        break;


    case 'editerheureSupp';
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
            {
                $id = securiser($_GET['id']);
                $objSalarie = new SalarieManager();
                $resultat = $objSalarie->getElementById($id);
                $objcongeHeureSupp = new HeureSuppManager();
                $resulatHeureSupps =  $objcongeHeureSupp->getElementByIdSalarie($id);
                $somme = $objcongeHeureSupp->getSumHeureSupp($id)->somme;
                include('views/heureSupp/editerHeureSupp.php');  
            }
        }else
        {
            include('views/salarie/connexion.php');
        }  
    break;


    case 'planing':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin'||$_SESSION['autorisation']=='salarie')
                {
                    $objSalarie=new SalarieManager();
                    $agents= $objSalarie->afficherTout();
                    $date = new Month();
                    if (isset($_GET['year'])){
                        $year= $_GET['year'];
                    }elseif(isset($_POST['year'])){
                        $year = $_POST['year'];
                    }else{
                        $year = date('Y');
                    }
                    $dates = $date->getAll($year);
                    $datesSeul=$dates[$year] ;
                    $objcongeAcquis = new CongeAcquisManager();
                    $resulatCongeAcquis = $objcongeAcquis->afficherTout();
                    $objConge = new CongeManager();
                    $resultatConges = $objConge->afficherTout();
                   
                    include('views/salarie/planing.php');
                    
                }
        }else{
            include('views/salarie/connexion.php');
        }
        break;

    case 'depotConge':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin'||$_SESSION['autorisation']=='salarie')
            {
                $objConge = new CongeManager();
                $resultatConges = $objConge->afficherTout();
                include('views/salarie/depotConge.php');
            }
        }else
        {
            include('views/salarie/connexion.php');
        }
        break;

    case 'listeCongeSoumis':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin'||$_SESSION['autorisation']=='salarie')
            {
                $objCongeAcquis = new CongeAcquisManager();
                $resulatCongeAcquis = $objCongeAcquis->getInvalideCongeJoinSalarie();
                include('views/salarie/listeCongeSoumis.php');
            }
        }else
        {
            include('views/salarie/connexion.php');
        }
        break;


    case 'listeCongeSalarie':
        if (isset($_SESSION['autorisation']))
        {
            $idSalarie = $_SESSION['idSalarie'];
            $objCongeAcquis = new CongeAcquisManager();
            $resulatCongeAcquis = $objCongeAcquis->afficherCongesSalarie($idSalarie);
            include('views/salarie/listeCongeSalarie.php');
        }else
        {
            include('views/salarie/connexion.php');
        }
        break;

    case 'listeHeureSupp':
        if (isset($_SESSION['autorisation']))
        {
            $id = $_SESSION['idSalarie'];
            $objSalarie = new SalarieManager();
            $resultat = $objSalarie->getElementById($id);
            $objcongeHeureSupp = new HeureSuppManager();
            $resulatHeureSupps =  $objcongeHeureSupp->getElementByIdSalarie($id);
            include('views/salarie/listeHeureSupp.php');
        }else
        {
            include('views/salarie/connexion.php');
        }
        break;

    case 'miseNiveau':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
            {
                
                $objSalarieConge = new SalarieCongeManager();
               
                //recuperation des jours CA non consommés
                $tableauCumules = [];
                $resultatSalarieConges = $objSalarieConge->getElementByIdConge(1);
                foreach ($resultatSalarieConges as $resultatSalarieConge)
                {
                   $tableauCumules[$resultatSalarieConge->getIdSalarie()] = $resultatSalarieConge->getCumule();
                }
                 //recuperation des jours CET non consommés
                $tableauCumuleCET = [];
                $resultatSalarieCongeCET = $objSalarieConge->getElementByIdConge(10);
                foreach ($resultatSalarieCongeCET as $resultatSalarieConge)
                {
                   $tableauCumuleCET[$resultatSalarieConge->getIdSalarie()] = $resultatSalarieConge->getCumule();
                }
                 //recuperation des jours RECUP non consommés
                $tableauCumuleRECUP = [];
                $resultatSalarieCongeRECUP = $objSalarieConge->getElementByIdConge(7);
                foreach ($resultatSalarieCongeRECUP as $resultatSalarieConge)
                {
                   $tableauCumuleRECUP[$resultatSalarieConge->getIdSalarie()] = $resultatSalarieConge->getCumule();
                }
                //remettre à jour les autres congés 
               $resultatSalarieConges = $objSalarieConge->afficherTout();
               $objConge = new CongeManager();
               $resultatConges = $objConge->afficherTout();
               foreach($resultatSalarieConges as $resultatSalarieConge)
               {
                   foreach($resultatConges as $resultatConge)
                   {
                       if($resultatSalarieConge->getIdConge()==$resultatConge->getIdConge())
                       {
                           $salarieConge = new SalarieConge();
                           $salarieConge->hydrate($resultatSalarieConge->getIdSalarie(),$resultatSalarieConge->getIdConge(),$resultatConge->getJourAttribue());
                           $resultat=$objSalarieConge->modifierCumuleJour($salarieConge);
                       }
                   }
               }
               //ingection des CA dans RELEQUAT
               foreach($resultatSalarieConges as $resultatSalarieConge)
               {
                   foreach($tableauCumules as $cle =>$valeur){
                       if($resultatSalarieConge->getIdSalarie()==$cle)
                       {
                           $salarieConge->hydrate($cle,9,$valeur);
                           $objSalarieConge->modifierCumuleJour($salarieConge);
                       }
                   }
               }
               //reingection des CET
               foreach($resultatSalarieConges as $resultatSalarieConge)
               {
                   foreach($tableauCumuleCET as $cle =>$valeur){
                       if($resultatSalarieConge->getIdSalarie()==$cle)
                       {
                           $salarieConge->hydrate($cle,10,$valeur);
                           $objSalarieConge->modifierCumuleJour($salarieConge);
                       }
                   }
               }
               //reingection des RECUP
               foreach($resultatSalarieConges as $resultatSalarieConge)
               {
                   foreach($tableauCumuleRECUP as $cle =>$valeur){
                       if($resultatSalarieConge->getIdSalarie()==$cle)
                       {
                           $salarieConge->hydrate($cle,7,$valeur);
                           $objSalarieConge->modifierCumuleJour($salarieConge);
                       }
                   }
               }
               
               $objCongeAcquis = new CongeAcquisManager();
               $resulatCongeAcquis = $objCongeAcquis->miseAJourAnuelle();
               if($resulatCongeAcquis)
               {
                    $_SESSION['success']= 'La mise à jour a bien été effectué';
                    header('location:index.php?path=salarie&action=gererCongeSalarie'); 
               }
              
            }
        }else
        {
            include('views/salarie/connexion.php');
        }
        break;

    case 'miseNiveauRelequat':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    $objSalarieConge = new SalarieCongeManager();
                    $resultatSalarieConge = $objSalarieConge->miseANiveauRELIQUAT();
                    if($resultatSalarieConge)
                    {
                        $_SESSION['success']= 'La mise à jour a bien été effectué';
                        header('location:index.php?path=salarie&action=gererCongeSalarie');
                    }
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;



    case 'gererHistotique':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    include('views/salarie/gererHistorique.php');
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;


    case 'historiqueConge':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    if (isset($_POST['annee']))
                    {
                        $annee = $_POST['annee'];
                    }else{
                        $annee= 0;
                    }
                    $idSalarie = $_GET['id'];
                    $objCongeAcquis = new CongeAcquisManager();
                    $resulatCongeAcquis = $objCongeAcquis->afficherHistorique($idSalarie);
                    $objSalarie = new SalarieManager();
                    $resultatSalarie = $objSalarie->getElementById($idSalarie);
                    $objConge = new CongeManager();
                    $resulatConges = $objConge->afficherTout();
                    include('views/congeAcquis/historique.php');
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;    

    case 'api':
        if (isset($_SESSION['autorisation'])&&$_SESSION['autorisation']=='admin')
            {
                header('Access-Control-Allow-Origin: *');
                header('content-type: application/json;charset=UTF-8');
                $objSalarie= new SalarieManager();
                $resultats = $objSalarie->afficherToutapi();
                if (isset($_GET['key'])&&$_GET['key']=='123456789')
                {
                    echo json_encode($resultats);
                }
       
        }
        break;

    
    case 'apiJourFerie':
        if (isset($_SESSION['autorisation'])&&$_SESSION['autorisation']=='admin')
        {
            header('Access-Control-Allow-Origin: *');
            header('content-type: application/json;charset=UTF-8');
            $objMonth = new Month();
            
            if (isset($_GET['key'])&&$_GET['key']=='123456789'){
                if(isset($_GET['year'])){
                $year = securiser($_GET['year']);
                $resultats = $objMonth->getJourFerie($year);
                echo json_encode($resultats);}
                if (isset($_GET['yearDebut'])||isset($_GET['yearFin'])){
                    $tableauJourFerier=[];
                    $yearDebut = securiser($_GET['yearDebut']);
                    $yearFin = securiser($_GET['yearFin']);
                    $resultatDebut = $objMonth->getJourFerie($yearDebut);
                    $resultatFin = $objMonth->getJourFerie($yearFin);
                    foreach( $resultatDebut as $cle =>$valeur){
                        $tableauJourFerier[$cle]=$valeur;
                    }
                    foreach( $resultatFin as $cle =>$valeur){
                        $tableauJourFerier[$cle]=$valeur;
                    }
                    echo json_encode($tableauJourFerier);
                }
            }
            
        }
        break;


    case '404':
        
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin'|| $_SESSION['autorisation'] = 'salarie')
                {
                    include('views/fragments/page404.php');
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;
        
        

    }