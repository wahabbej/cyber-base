<?php

if (isset($_GET['action']))
{
    $action = securiser($_GET['action']);
}


switch ($action)
{
    case 'listeConge':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    $objConge = new CongeManager();
                    $resultatconges = $objConge->afficherTout();
                    include('views/conge/listeConge.php');
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;


    case 'ajoutConge':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    include('views/conge/ajoutConge.php');
                } 
        }else
        {
            include('views/salarie/cnnexion.php');
        }break;


    case 'confirmAjout':
        if (isset($_SESSION['autorisation']))
        {
            $tokenT = $_POST['tokenT'];
           
            if( $_SESSION['autorisation']=='admin'&&$tokenT==$token)
                {
                    $libelle = securiser($_POST['libelle']);
                    $code = securiser($_POST['code']);
                    $codeCouleur = securiser($_POST['codeCouleur']);
                    $attribue=securiser($_POST['attribue']);
                    $conge = new Conge();
                    $conge->hydrate($libelle,$code,$codeCouleur,$attribue);
                    $objConge = new CongeManager();
                    $resultat = $objConge->ajoutConge($conge);
                    if ($resultat)
                    {
                        $_SESSION['success'] =  'Le congé a bien été enregistré';
                        header('location:index.php?path=conge&action=listeConge');
                    }
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;


    case 'profileConge':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    $id = securiser($_GET['id']);
                    $objConge = new CongeManager();
                    $resultats = $objConge->getElementById($id);
                    include('views/conge/profileConge.php');
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;


    case 'modifConge':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    $id = securiser($_GET['id']);
                    $objConge = new CongeManager();
                    $resultats = $objConge->getElementById($id);
                    include('views/conge/modifConge.php');
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;


    case 'confirmerModif':
        if (isset($_SESSION['autorisation']))
        {
            $tokenT = $_POST['tokenT'];
           
            if( $_SESSION['autorisation']=='admin'&&$tokenT==$token)
                {
                    $id = securiser($_POST['id']);
                    $libelle = securiser($_POST['libelle']);
                    $code= securiser($_POST['code']);
                    $codeCouleur = securiser($_POST['codeCouleur']);
                    $jourAttribue = securiser($_POST['jourAttribue']);
                    $conge = new Conge();
                    $conge->hydrate($libelle,$code,$codeCouleur,$jourAttribue);
                    $conge->setIdConge($id);
                    $objConge = new CongeManager();
                    $resultat = $objConge->modiffierConge($conge);
                    if($resultat){
                        $_SESSION['success'] = 'Le congé a bien été modifié';
                        header('location:index.php?path=conge&action=profileConge&id='.$id);
                    }
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;


    case 'supprimerConge':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    $id = securiser($_GET['id']) ;
                    $objConge = new CongeManager();
                    $resultat = $objConge->getElementById($id);
                    if($resultat){
                    $resultat = $objConge->supprimerConge($id);
                    if ($resultat){
                        $_SESSION['success'] = 'Le congé a bien été supprimé';
                        header('location:index.php?path=conge&action=listeConge');
                    }}
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;


    case 'calendrier':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin'||$_SESSION['autorisation']=='salarie')
                {
                    $objMonth = new Month();
                        
                        $date = new Month();
                    if (isset($_GET['year'])){
                        $year= $_GET['year'];
                    }elseif(isset($_POST['year'])){
                        $year = $_POST['year'];
                    }else{
                        $year = date('Y');
                    }
                    $jourFeries = $objMonth->getJourFerie($year);
                    $dates = $date->getAll($year);
                    $datesSeul=$dates[$year] ;
                    include('views/conge/calendrier.php');
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;
        
    
}
