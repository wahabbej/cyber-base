<?php

if (isset($_GET['action']))
{
    $action = securiser($_GET['action']);
}


switch ($action)
{
   case 'ajoutCongeSalarie':
    if (isset($_SESSION['autorisation']))
        {
            $tokenT = $_POST['tokenT'];
            if( $_SESSION['autorisation']=='admin'&& $tokenT==$token)
                {
                    if(!empty($_POST['dateDebut'])&&!empty($_POST['dateFin'])&&!empty($_POST['idConge']))
                    {
                        $dateDebut=securiser($_POST['dateDebut']);
                        $dateFin=securiser($_POST['dateFin']);
                        $idSalarie=securiser($_POST['idSalarie']);
                        $idConge=securiser($_POST['idConge']);
                        $acquis= congeSeul($dateDebut,$dateFin);

                        if (verfDate($dateDebut,$dateFin)==true)
                        {
                            $_SESSION['flash']['danger']= 'La date de début doit être inferieur à celle de la fin';
                            header('location:index.php?path=salarie&action=editerconge&id='.$idSalarie);
                        }else
                        {
                                //traitement pour les heures supp
                            if($idConge==7){
                                $objSalarie = new SalarieManager();
                                $resultatSalarie = $objSalarie->getElementById($idSalarie);
                                $nbJour= $resultatSalarie->getNbJourSemaine();
                                $nbHeureSemaine = $resultatSalarie->getTauxHoraire();
                                $tHJ = $nbHeureSemaine/$nbJour;
                                $objHeureSupp = new HeureSuppManager();
                                $nbMinute = -1*($acquis*$tHJ*60) ;
                                $heureSupp = new HeureSupp();
                                $heureSupp->hydrate(null,null,null,$nbMinute,$idSalarie);
                                $objHeureSupp->ajoutHeureSupp( $heureSupp);
                            }

                            //traitement jes JF
                            if($idConge==1){
                            if (transformDate($dateFin)->format('m')<5|| transformDate($dateDebut)->format('m')>10){
                                if($acquis>4 && $acquis<9){
                                    $objSalarieConge = new SalarieCongeManager();
                                    $cumuleAncien = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,3);
                                    $cumule =$cumuleAncien+1;
                                    $salarieConge = new SalarieConge();
                                    $salarieConge->hydrate($idSalarie,3,$cumule);
                                    $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);
                                }elseif($acquis>8){
                                    $objSalarieConge = new SalarieCongeManager();
                                    $cumuleAncien = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,3);
                                    $cumule =$cumuleAncien+2;
                                    $salarieConge = new SalarieConge();
                                    $salarieConge->hydrate($idSalarie,3,$cumule);
                                    $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);
                                }
                                
                            }
                        }
                            $congeAquis=new CongeAcquis();
                            $congeAquis->hydrate($idSalarie,$idConge,$dateDebut,$dateFin,$acquis,1);
                            $objCongeAquis= new CongeAcquisManager();
                            $resultatCongeAquis=$objCongeAquis->ajoutCongeAcquis($congeAquis);
                            $objSalarieConge = new SalarieCongeManager();
                            $cumuleAncien = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,$idConge);
                            $cumule =$cumuleAncien-$acquis;
                            $salarieConge = new SalarieConge();
                            $salarieConge->hydrate($idSalarie,$idConge,$cumule);
                            $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);
                            
                        
                                $_SESSION['flash']['success']= 'Le congé a bien été pris en compte ';
                                header('location:index.php?path=salarie&action=editerconge&id='.$idSalarie);
                        }
                    }else
                    {
                        $_SESSION['flash']['danger']= 'tous les champs sont requis ';
                        header('location:index.php?path=salarie&action=editerconge&id='.$idSalarie);
                    }
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break; 
        

    case 'profileCongeAcquis':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    $idSalarie = securiser($_GET['idSalarie']);
                    $id = securiser($_GET['id']);
                    $objCongeAquisManager = new CongeAcquisManager();
                    $resultats= $objCongeAquisManager->getElementById($id);
                    if(!$resultats)
                    {
                        header("location:index.php?path=salarie&action=editerconge&id=".$idSalarie);
                    }
                    $objcongeManager= new CongeManager();
                    $resultatConges=$objcongeManager->afficherTout();
                    $objSalarieManager= new SalarieManager();
                    $resultatSalaries= $objSalarieManager->afficherTout();
                    include('views/congeAcquis/profileCongeAcquis.php');
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;
          
           
    case 'modifCongeAcquis':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    $id = securiser($_GET['id']);
                    $objCongeAquisManager= new CongeAcquisManager();
                    $resultatCongeAquis=$objCongeAquisManager->getElementById($id);
                    $objcongeManager= new CongeManager();
                    $resultatConges=$objcongeManager->afficherTout();
                    include('views/congeAcquis/modifierCongeAcquis.php');
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;
        


    case 'confirmModifCongeAcquis':
        if (isset($_SESSION['autorisation']))
        {
            $tokenT = $_POST['tokenT'];
            if( $_SESSION['autorisation']=='admin'&&$tokenT==$token)
                {
                    $debutAncien = securiser($_POST['debutAncien']);
                    $finAncien = securiser($_POST['finAncien']);
                    $congeAncien = securiser($_POST['congeAncien']);
                    $decompteAncien = securiser($_POST['decomptet0']);
                    $idCongeAcquis= securiser($_POST['idCongeAcquis']);
                    $dateDebut = securiser($_POST['dateDebut']) ;
                    $dateFin = securiser($_POST['dateFin']) ;
                    if($_POST['decomptet']==''){
                        $decompte= securiser($_POST['decomptet0']) ;}else{
                        $decompte= congeSeul($dateDebut,$dateFin) ;
                    }
                    $idSalarie = securiser($_POST['idSalarie']);
                    $idConge= securiser($_POST['idConge']);
                    
                    if (verfDate($dateDebut,$dateFin)==true)
                    {
                        $_SESSION['flash']['danger']= 'La date de début doit être inferieur à celle de la fin';
                        header('location:index.php?path=congeAquis&action=modifCongeAcquis&id='.$idCongeAcquis);
                    }else
                    {
                        //traitement mise a jour des heures supp 
                        if($idConge==7){
                            
                            $objSalarie = new SalarieManager();
                            $resultatSalarie = $objSalarie->getElementById($idSalarie);
                            $nbJour= $resultatSalarie->getNbJourSemaine();
                            $nbHeureSemaine = $resultatSalarie->getTauxHoraire();
                            $tHJ = $nbHeureSemaine/$nbJour;
                            $objHeureSupp = new HeureSuppManager();
                            $resultatHeureSupp = $objHeureSupp->getSumHeureSupp($idSalarie);
                            $somme = $resultatHeureSupp->somme;
                            $nbMinute = ($decompteAncien- $decompte)*$tHJ*60;
                            $heureSupp = new HeureSupp();
                            $heureSupp->hydrate(null,null,null,$nbMinute,$idSalarie);
                            $objHeureSupp->ajoutHeureSupp( $heureSupp);
                            
                            
                        }
                    

                        //traitement pour mettre a jours le JF
                        if($congeAncien==1){
                            if (transformDate($finAncien)->format('m')<5|| transformDate($debutAncien)->format('m')>10){
                                if($decompteAncien>4 && $decompteAncien<9){
                                    $objSalarieConge = new SalarieCongeManager();
                                    $cumuleAncien = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,3);
                                    $cumule =$cumuleAncien-1;
                                    $salarieConge = new SalarieConge();
                                    $salarieConge->hydrate($idSalarie,3,$cumule);
                                    $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);
                                }elseif($decompteAncien>8){
                                    $objSalarieConge = new SalarieCongeManager();
                                    $cumuleAncien = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,3);
                                    $cumule =$cumuleAncien-2;
                                    $salarieConge = new SalarieConge();
                                    $salarieConge->hydrate($idSalarie,3,$cumule);
                                    $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);
                                }
                                
                            }
                        }

                        // // //traitement pour remetre a jour le cummule avant l'ajout de ce congé
                        $objSalarieConge = new SalarieCongeManager();
                        $cumuleAncien = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,$congeAncien);
                        $cumule =$cumuleAncien + $decompteAncien;
                        $salarieConge = new SalarieConge();
                        $salarieConge->hydrate($idSalarie,$congeAncien,$cumule);
                        $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);

                        //traitement pour mettre a jour les cumules de congé apres la modification 

                        if($idConge==1){
                            if (transformDate($dateFin)->format('m')<5|| transformDate($dateDebut)->format('m')>10){
                                if($decompte>4 && $decompte<9){
                                    $objSalarieConge = new SalarieCongeManager();
                                    $cumuleAncien = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,3);
                                    $cumule =$cumuleAncien+1;
                                    $salarieConge = new SalarieConge();
                                    $salarieConge->hydrate($idSalarie,3,$cumule);
                                    $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);
                                }elseif($decompte>8){
                                    $objSalarieConge = new SalarieCongeManager();
                                    $cumuleAncien = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,3);
                                    $cumule =$cumuleAncien+2;
                                    $salarieConge = new SalarieConge();
                                    $salarieConge->hydrate($idSalarie,3,$cumule);
                                    $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);
                                }
                                
                            }
                        }

                        $congeAquis= new CongeAcquis();
                        $congeAquis->hydrate($idSalarie,$idConge,$dateDebut,$dateFin,$decompte,1);
                        $congeAquis->setIdCongeAcquis($idCongeAcquis);
                        $objCongeAquisManager = new CongeAcquisManager();
                        $resultatCongeAquis= $objCongeAquisManager->modifierCongAcquis($congeAquis);

                        $objSalarieConge = new SalarieCongeManager();
                        $cumuleAncien = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,$idConge);
                        $cumule =$cumuleAncien - $decompte;
                        $salarieConge = new SalarieConge();
                        $salarieConge->hydrate($idSalarie,$idConge,$cumule);
                        $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);
                        

                        
                        if($resultatCongeAquis){
                            $_SESSION['flash']['success']= 'Les dates de congé ont bien été modifiées ';
                        header('location:index.php?path=congeAquis&action=profileCongeAcquis&id='.$idCongeAcquis.'&idSalarie='.$idSalarie);
                        }
                }
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;  
            

    case 'supprimerCongeAcquis':
        if (isset($_SESSION['autorisation']))
        {
            $tokenT = $_POST['tokenT'];
            if( $_SESSION['autorisation']=='admin'&&$tokenT==$token)
                {
                    $id =securiser($_POST['id']);
                    $idSalarie = securiser($_POST['idSalarie']); 
                    $idConge = securiser($_POST['idConge']);
                    $decompte =securiser($_POST['decompte']);
                    $dateDebut = securiser($_POST['dateDebut']);
                    $dateFin = securiser($_POST['dateFin']);

                    //traitement heures supp
                    if($idConge==7){
                    
                        $objSalarie = new SalarieManager();
                        $resultatSalarie = $objSalarie->getElementById($idSalarie);
                        $nbJour= $resultatSalarie->getNbJourSemaine();
                        $nbHeureSemaine = $resultatSalarie->getTauxHoraire();
                        $tHJ = $nbHeureSemaine/$nbJour;
                        $objHeureSupp = new HeureSuppManager();
                        $resultatHeureSupp = $objHeureSupp->getSumHeureSupp($idSalarie);
                        $somme = $resultatHeureSupp->somme;
                        $nbMinute = ($decompte)*$tHJ*60;
                        $heureSupp = new HeureSupp();
                        $heureSupp->hydrate(null,null,null,$nbMinute,$idSalarie);
                        $objHeureSupp->ajoutHeureSupp( $heureSupp);
                        
                        
                    }
                    
                    
                    if($idConge==1){
                        if (transformDate($dateFin)->format('m')<5|| transformDate($dateDebut)->format('m')>10){
                            
                            if($decompte>4 && $decompte<9){
                                $objSalarieConge = new SalarieCongeManager();
                                $cumuleAncien = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,3);
                                $cumule =$cumuleAncien-1;
                                $salarieConge = new SalarieConge();
                                $salarieConge->hydrate($idSalarie,3,$cumule);
                                $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);
                            }elseif($decompte>8){
                                $objSalarieConge = new SalarieCongeManager();
                                $cumuleAncien = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,3);
                                $cumule =$cumuleAncien-2;
                                $salarieConge = new SalarieConge();
                                $salarieConge->hydrate($idSalarie,3,$cumule);
                                $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);
                            }
                            
                        }
                    }

                    
                    $objSalarieConge = new SalarieCongeManager();
                    $cumuleAncien = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,$idConge);
                    $cumule =$cumuleAncien + $decompte;
                    $salarieConge = new SalarieConge();
                    $salarieConge->hydrate($idSalarie,$idConge,$cumule);
                    $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);


                    $objCongeAquisManager= new CongeAcquisManager();
                    $resultatCongeAquis= $objCongeAquisManager->supprimerCongeAcquis($id);
                    if($resultatCongeAquis){
                        $_SESSION['flash']['success']= 'Le congé a bien été supprimé';
                        header('location:index.php?path=salarie&action=editerconge&id='.$idSalarie);
                    }
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;

        
    case 'depotConge':
        if (isset($_SESSION['autorisation']))
        {
            $tokenT = $_POST['tokenT'];
            if($tokenT==$token)
            {
                if (!empty(($_POST['dateDebut']))&&!empty($_POST['dateFin'])&&!empty($_POST['idConge']))
                {
                    $dateDebut = securiser($_POST['dateDebut']);
                    $dateFin = securiser($_POST['dateFin']);
                    $idConge = securiser($_POST['idConge']);
                    $idSalarie = $_SESSION['idSalarie'];
                    if (verfDate($dateDebut,$dateFin)==true)
                        {
                            $_SESSION['flash']['danger']= 'La date du début de congé doit être inferieur à celle de la fin';
                            header('location:index.php?path=salarie&action=depotConge');
                    }else
                        {
                            $acquis = congeSeul($dateDebut,$dateFin);
                            $objCongeAquis = new CongeAcquisManager();
                            $congeAquis = new CongeAcquis();
                            $congeAquis->hydrate($idSalarie,$idConge,$dateDebut,$dateFin,$acquis,0);
                            $resultatCongeAquis = $objCongeAquis->ajoutCongeAcquis($congeAquis);
                            if($resultatCongeAquis)
                            {
                                $_SESSION['flash']['success'] = 'Votre demande de congé a bien été prise en compte, elle sera taraiter dans les plus bref délais';
                                header('location:index.php?path=salarie&action=depotConge');
                            }
                        }
                    }
                }
        }else
        {
            include('views/salarie/connexion.php');
        }break;
           

    case 'accepterConge':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    $idCongeAcquis = securiser($_POST['idCongeAcquis']);
                    $dateDebut=securiser($_POST['dateDebut']);
                    $dateFin=securiser($_POST['dateFin']);
                    $idSalarie=securiser($_POST['idSalarie']);
                    $idConge=securiser($_POST['idConge']);
                    $acquis= congeSeul($dateDebut,$dateFin);
                    //traitement pour les heures supp
                    if($idConge==7){
                        $objSalarie = new SalarieManager();
                        $resultatSalarie = $objSalarie->getElementById($idSalarie);
                        $nbJour= $resultatSalarie->getNbJourSemaine();
                        $nbHeureSemaine = $resultatSalarie->getTauxHoraire();
                        $tHJ = $nbHeureSemaine/$nbJour;
                        $objHeureSupp = new HeureSuppManager();
                        $nbMinute = -1*($acquis*$tHJ*60) ;
                        $heureSupp = new HeureSupp();
                        $heureSupp->hydrate(null,null,null,$nbMinute,$idSalarie);
                        $objHeureSupp->ajoutHeureSupp( $heureSupp);
                    }

                    //traitement jes JF
                    if($idConge==1){
                    if (transformDate($dateFin)->format('m')<5|| transformDate($dateDebut)->format('m')>10){
                        if($acquis>4 && $acquis<9){
                            $objSalarieConge = new SalarieCongeManager();
                            $cumuleAncien = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,3);
                            $cumule =$cumuleAncien+1;
                            $salarieConge = new SalarieConge();
                            $salarieConge->hydrate($idSalarie,3,$cumule);
                            $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);
                        }elseif($acquis>8){
                            $objSalarieConge = new SalarieCongeManager();
                            $cumuleAncien = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,3);
                            $cumule =$cumuleAncien+2;
                            $salarieConge = new SalarieConge();
                            $salarieConge->hydrate($idSalarie,3,$cumule);
                            $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);
                        }
                        
                    }
                }

                    $objCongeAquis = new CongeAcquisManager();
                    $resultatCongeAquis = $objCongeAquis->accepterConge($idCongeAcquis);
                    $objSalarieConge = new SalarieCongeManager();
                    $cumuleAncien = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,$idConge);
                    $cumule =$cumuleAncien-$acquis;
                    $salarieConge = new SalarieConge();
                    $salarieConge->hydrate($idSalarie,$idConge,$cumule);
                    $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);
                    if($resultatSalarieConge)
                    {
                        $_SESSION['flash']['success'] = 'Le congé a bien été pris en compte';
                        header('location:index.php?path=salarie&action=listeCongeSoumis');
                    }
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;

            
        

    case 'rejetConge':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    $idCongeAcquis = securiser($_GET['idCongeAcquis']);
                    $objCongeAquis = new CongeAcquisManager();
                    $resultatCongeAquis = $objCongeAquis->supprimerCongeAcquis($idCongeAcquis);
                    if($resultatCongeAquis)
                    {
                        $_SESSION['flash']['danger'] = 'Le congé a été rejeté ';
                        header('location:index.php?path=salarie&action=listeCongeSoumis');
                    }
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;

    }


 