<?php 
$action ='home';
if (isset($_GET['action']))
{
    $action = securiser($_GET['action']);
}


switch ($action)
{
    case 'ajoutHeureSupp':
        if (isset($_SESSION['autorisation']))
        {
            $tokenT = $_POST['tokenT'];
            if( $_SESSION['autorisation']=='admin'&& $tokenT==$token)
                {
                    $idSalarie = securiser($_POST['idSalarie']);
                    if(!empty($_POST['dateDebut'])&&!empty($_POST['dateFin']))
                    {
                        $cumuleJours= securiser($_POST['cumulJours']);
                        $heureDebut =transformDate(securiser($_POST['dateDebut']))->format('H:i') ;
                        $heureFin = transformDate(securiser($_POST['dateFin']))->format('H:i');
                        $date = transformDate( securiser($_POST['dateDebut']))->format('Y-m-d');
                        $nbMinute = diffMinute(securiser($_POST['dateDebut']),securiser($_POST['dateFin']));

                        if (verfDate($heureDebut,$heureFin)==true)
                        {
                        
                            $_SESSION['flash']['danger']= 'La date de début doit être inferieur à celle de la fin';
                            header('location:index.php?path=salarie&action=editerheureSupp&id='.$idSalarie);
                        }
                        else
                        {
                            $heureSupp = new HeureSupp();
                            $heureSupp->hydrate($date,$heureDebut,$heureFin,$nbMinute,$idSalarie);
                            $objHeureSupp = new HeureSuppManager();
                            $resultats = $objHeureSupp->ajoutHeureSupp($heureSupp);

                        
                            if($resultats)
                            {
                                $objHeureSupp = new HeureSuppManager();
                                $resultatHeureSupp = $objHeureSupp->getSumHeureSupp($idSalarie);
                                $somme = $resultatHeureSupp->somme;
                                $objSalarie = new SalarieManager();
                                $resultatSalarie = $objSalarie->getElementById($idSalarie);
                                $tSH = $resultatSalarie->getTauxHoraire()/$resultatSalarie->getNbJourSemaine();
                                $cumuleJours = floor(($somme/60)/$tSH);      
                            
                                $salarieConge = new SalarieConge();
                                $salarieConge->hydrate($idSalarie,7,$cumuleJours);
                                $objSalarieConge = new SalarieCongeManager();
                                $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);
                                if($resultatSalarieConge)
                                {
                                    $_SESSION['flash']['success'] = 'Les heures supp ont bien été prises en compte ';
                                    header('location:index.php?path=salarie&action=editerheureSupp&id='.$idSalarie);
                                }   
                            }
                        }
                    }else
                    {
                        $_SESSION['flash']['danger'] = 'tous les champs sont requis ';
                        header('location:index.php?path=salarie&action=editerheureSupp&id='.$idSalarie);
                    }
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;


    case 'profileHeureSupp':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    $id = securiser($_GET['id']);
                    $idSalarie = securiser($_GET['idSalarie']);
                    $objHeureSupp = new HeureSuppManager();
                    $resultats= $objHeureSupp->getElementByIdHeureSupp($id);    
                    if(!$resultats)   
                    {
                        header("location:index.php?path=salarie&action=editerheureSupp&id=$idSalarie");
                    }            
                    $objSalarieManager= new SalarieManager();
                    $resultatSalarie= $objSalarieManager->getElementById($idSalarie);
                    include ('views/heureSupp/profileHeureSupp.php');   
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;


    case 'modifHeureSupp':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    $id = securiser($_GET['id']);
                    $objHeureSupp= new HeureSuppManager();
                    $resultatHeureSupp=$objHeureSupp->getElementByIdHeureSupp($id);
                    include('views/heureSupp/modifierHeureSupp.php');
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;


    case 'confirmModifHeureSupp':
           if (isset($_SESSION['autorisation']))
        {
            $tokenT = $_POST['tokenT'];
            if( $_SESSION['autorisation']=='admin'&& $tokenT==$token)
                {
                    $idHeureSupp = securiser($_POST['idHeureSupp']);
                    if(!empty($_POST['heureDebut'])&&!empty($_POST['heureFin']))
                    {
                        $heureDebut = transformDate(securiser($_POST['heureDebut']))->format('H:i') ;
                        $heureFin = transformDate(securiser($_POST['heureFin']))->format('H:i') ;
                        $date = transformDate( securiser($_POST['heureDebut']))->format('Y-m-d');
                        if($_POST['decomptet'] == '')
                            {
                                $decompte = securiser($_POST['decomptet0']) ;
                            }else{
                                $decompte = diffMinute(securiser($_POST['heureDebut']),securiser($_POST['heureFin'])); 
                            }
                        $idSalarie = securiser($_POST['idSalarie']);

                        if (verfDate($heureDebut,$heureFin)==true)
                        {
                            $_SESSION['flash']['danger']= 'L\'heure de début doit être inferieur à celle de la fin';
                            header('location:index.php?path=heureSupp&action=modifHeureSupp&id='.$idHeureSupp);
                        }else
                        {
                            $heureSupp= new HeureSupp();
                            $heureSupp->hydrate($date,$heureDebut,$heureFin,$decompte,$idSalarie);
                            $heureSupp->setIdHeureSupp($idHeureSupp);
                        
                            $objHeureSupp = new HeureSuppManager();
                            $resultatHeureSupp= $objHeureSupp->modifierHeureSupp($heureSupp);
                            if($resultatHeureSupp){
                                $resultatSum = $objHeureSupp->getSumHeureSupp($idSalarie);
                                $somme = $resultatSum->somme;
                                $objSalarie = new SalarieManager();
                                $resultatSalarie = $objSalarie->getElementById($idSalarie);
                                $tSH = $resultatSalarie->getTauxHoraire()/$resultatSalarie->getNbJourSemaine();
                                $cumuleJours = floor(($somme/60)/$tSH);
                                $salarieConge = new SalarieConge();
                                $salarieConge->hydrate($idSalarie,7,$cumuleJours);
                                $objSalarieConge = new SalarieCongeManager();
                                $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);

                            $_SESSION['flash']['success']= 'Les heures supp ont bien été modifiées ';
                            header('location:index.php?path=heureSupp&action=profileHeureSupp&id='.$idHeureSupp.'&idSalarie='.$idSalarie);
                            }
                        } 
                    }else{
                        $_SESSION['flash']['danger']= 'tous les champs sont requis ';
                        header("location:index.php?path=heureSupp&action=modifHeureSupp&id=$idHeureSupp");
                    }
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;   
                
           
    case 'supprimerHeureSupp':
        if (isset($_SESSION['autorisation']))
            {
                $tokenT = $_POST['tokenT'];
                if( $_SESSION['autorisation']=='admin'&& $tokenT==$token)
                    {
                        $idHeureSupp= securiser($_GET['id']);
                        $idSalarie= securiser($_GET['idSalarie']);
                        $objHeureSupp= new HeureSuppManager();
                        $resultatHeureSupp= $objHeureSupp->supprimerHeureSupp($idHeureSupp,$idSalarie);
                        if($resultatHeureSupp)
                        {
                            $resultatSum = $objHeureSupp->getSumHeureSupp($idSalarie);
                            $somme = $resultatSum->somme;


                            $objSalarie = new SalarieManager();
                            $resultatSalarie = $objSalarie->getElementById($idSalarie);
                            $tSH = $resultatSalarie->getTauxHoraire()/$resultatSalarie->getNbJourSemaine();
                            $cumuleJours = floor(($somme/60)/$tSH);


                            $salarieConge = new SalarieConge();
                            $salarieConge->hydrate($idSalarie,7,$cumuleJours);
                            $objSalarieConge = new SalarieCongeManager();
                            $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);

                            $_SESSION['flash']['success']= 'l\'heure supp ont bien été supprimée';
                            header('location:index.php?path=salarie&action=editerheureSupp&id='.$idSalarie);
                        }
                    } 
            }else
            {
                include('views/salarie/connexion.php');
            }break;


    case 'ajoutJour':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    $idSalarie = securiser($_POST['idSalarie']);
                    $jours = securiser($_POST['jours']);
                    $objSalarieConge = new SalarieCongeManager();
                    $ancienCumule = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,7);
                    $nouveauCumule = $ancienCumule+$jours;
                    $salarieConge = new SalarieConge();
                    $salarieConge->hydrate($idSalarie,7,$nouveauCumule);
                    $resultatSalarieConge =  $objSalarieConge->modifierCumuleJour($salarieConge);
                    if($resultatSalarieConge)
                    {
                        $objSalarie = new SalarieManager();
                        $resultatSalarie = $objSalarie->getElementById($idSalarie);
                        $nbJour= $resultatSalarie->getNbJourSemaine();
                        $nbHeureSemaine = $resultatSalarie->getTauxHoraire();
                        $tHJ = $nbHeureSemaine/$nbJour;
                        $nbMinute = $tHJ * $jours * 60;
                        $heureSupp= new HeureSupp();
                        $heureSupp->hydrate(null,null,null,$nbMinute,$idSalarie);
                        $objHeureSupp = new HeureSuppManager();
                        $resultatHeureSupp = $objHeureSupp->ajoutHeureSupp($heureSupp);
                        if ($resultatHeureSupp)
                        {
                            $_SESSION['flash']['success'] = 'le(s) jour(s) RECUP a bien été pris en compte ';
                            header('location:index.php?path=salarie&action=editerheureSupp&id='.$idSalarie);
                        }
                    }
                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;      
               
}