<?php 
if (isset($_GET['action']))
{
    $action = securiser($_GET['action']);
}


switch ($action)
{
    case 'editerCumuleConge': 
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    $idSalarie = securiser($_GET['id']);
                    $objSalarieConge = new SalarieCongeManager();
                    $resultatSalarieConges = $objSalarieConge->affichageCumuleCongeJoinCongeSalarie($idSalarie);
                    $objsalarie = new SalarieManager();
                    $resultatSalarie = $objsalarie->getElementById($idSalarie);
                    include ('views/cumuleConge/cumuleConge.php');
                }
               
        }
        else{
            include('views/salarie/connexion.php');
        }break;
       

    case 'modifierCumuleConge':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    $idSalarie = securiser($_GET['idSalarie']);
                $code = securiser($_GET['code']);
                if($code=='RECUP'){
                    $_SESSION['flash']["danger"]='Les modification de cumule de RECUP ne sont pas prises en compte. Pour se faire, il faut se rendre sur le lien des heures supplimentaires ';
                    header('location:index.php?path=cumuleConge&action=editerCumuleConge&id='.$idSalarie);
                }
                $objSalarieConge = new SalarieCongeManager();
                $resultatSalarieConge = $objSalarieConge->getElementByIdSalarieCodeConge($idSalarie,$code);
                $objsalarie = new SalarieManager();
                $resultatSalarie = $objsalarie->getElementById($idSalarie);
                include('views/cumuleConge/modifierCumuleConge.php');

                }
               
        }else
        {
            include('views/salarie/connexion.php');
        }break;

                
    case 'confirmerModif':
        if (isset($_SESSION['autorisation']))
        {
            $tokenT = $_POST['tokenT'];
           
            if( $_SESSION['autorisation']=='admin'&&$tokenT==$token)
                {
                    $idSalarie = securiser($_POST['idSalarie']);
                    $code = securiser($_POST['code']);
                    $cumule = securiser($_POST['cumule']);
                    $objSalarieConge = new SalarieCongeManager();
                    $resultatSalarieConge = $objSalarieConge->modifierCumuleJourJoinConge($idSalarie,$code,$cumule);
                    if ($resultatSalarieConge)
                    {
                        $_SESSION['flash']['success'] = 'La modification de cumule de congé a bien été pris en compte ';
                        header('location:index.php?path=cumuleConge&action=editerCumuleConge&id='.$idSalarie);
                    }
                }
               
        }else
        {
            include('views/salarie/connexion.php');
        }break;
     
    
    case 'ajoutCet':
        if (isset($_SESSION['autorisation']))
        {
            if( $_SESSION['autorisation']=='admin')
                {
                    $idSalarie= securiser($_GET['idSalarie']);
                    $objsalarie = new SalarieManager();
                    $resultatSalarie = $objsalarie->getElementById($idSalarie);
                    $objConge =  new CongeManager();
                    $resultatConges = $objConge->afficherTout();
                    include('views/cumuleConge/ajoutCet.php');
                }  
        }else
        {
            include('views/salarie/connexion.php');
        }break;


    case 'confirmAjout':
        if (isset($_SESSION['autorisation']))
        {
            $tokenT = $_POST['tokenT'];
            if( $_SESSION['autorisation']=='admin'&&$tokenT==$token)
                {
                    $nbJour = securiser($_POST['nbJour']) ;
                    $idConge = securiser($_POST['idConge']);
                    $idSalarie = securiser($_POST['idSalarie']);
                    $objSalarieConge = new SalarieCongeManager();
                    $resultatSalarieConge = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,$idConge);
                    $salarieConge = new SalarieConge();
                    $cumule = $resultatSalarieConge - $nbJour;
                    $salarieConge->hydrate($idSalarie,$idConge,$cumule);
                    $resultat = $objSalarieConge->modifierCumuleJour($salarieConge);
                    if($idConge==7)
                    {
                        $objsalarie = new SalarieManager();
                        $resultatSalarie = $objsalarie->getElementById($idSalarie);
                        $tHS = $resultatSalarie->getTauxHoraire()/$resultatSalarie->getNbJourSemaine();
                        $nbMinute = -($tHS*$nbJour*60);
                        $heureSupp = new HeureSupp();
                        $heureSupp->hydrate(null,null,null,$nbMinute,$idSalarie);
                        $objHeureSupp = new HeureSuppManager();
                        $resultatHeureSupp = $objHeureSupp->ajoutHeureSupp($heureSupp);
                        
                    }
                    $ancienCumule = $objSalarieConge->getElementByIdSalarieIdconge($idSalarie,10);
                    $cumule = $ancienCumule+$nbJour;
                    $salarieConge ->hydrate($idSalarie,10,$cumule);
                    $resultatSalarieConge = $objSalarieConge->modifierCumuleJour($salarieConge);
                    if($resultatSalarieConge)
                    {
                        $_SESSION['flash']['success'] = 'Les modifications ont bien été prises en compte ';
                        header('location:index.php?path=cumuleConge&action=ajoutCet&idSalarie='.$idSalarie);
                    }
                    

                } 
        }else
        {
            include('views/salarie/connexion.php');
        }break;
    
}