-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 03 juin 2022 à 15:08
-- Version du serveur : 10.4.22-MariaDB
-- Version de PHP : 8.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `cyber_base`
--

-- --------------------------------------------------------

--
-- Structure de la table `conge`
--

CREATE TABLE `conge` (
  `idConge` int(60) NOT NULL,
  `libelle` varchar(60) NOT NULL,
  `code` varchar(10) NOT NULL,
  `codeCouleur` varchar(60) NOT NULL,
  `jourAttribue` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `conge`
--

INSERT INTO `conge` (`idConge`, `libelle`, `code`, `codeCouleur`, `jourAttribue`) VALUES
(1, 'congé annuel', 'CA', '#ff0000', 33),
(2, 'recuperation temps travail', 'RTT', '#4869ee', 11),
(3, 'Jours de fractionnement', 'JF', '#00ff00', 0),
(4, 'Congés Exceptionnels', 'CE', '#d9d900', 6),
(5, 'Jour(s) de Formation', 'FOR', '#ff80c0', 10),
(7, 'Jour(s) de Récupération heure(s) sup.', 'RECUP', '#038091', 0),
(9, 'Jour(s) de CA non utilisé(s) l\'an dernier', 'RELIQUAT', '#00ffff', 0),
(10, 'Compte Epargne Temps', 'CET', '#21ba75', 0);

-- --------------------------------------------------------

--
-- Structure de la table `congeacquis`
--

CREATE TABLE `congeacquis` (
  `idCongeAcquis` int(60) NOT NULL,
  `dateDebut` datetime NOT NULL,
  `dateFin` datetime NOT NULL,
  `acquis` int(60) NOT NULL,
  `statut` tinyint(1) NOT NULL,
  `archive` tinyint(1) NOT NULL DEFAULT 0,
  `idSalarie` int(60) NOT NULL,
  `idConge` int(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `heuresupp`
--

CREATE TABLE `heuresupp` (
  `idHeureSupp` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `heureDebut` time DEFAULT NULL,
  `heureFin` time DEFAULT NULL,
  `nbMinute` int(11) NOT NULL,
  `pris` tinyint(1) NOT NULL,
  `idSalarie` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `nom` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id`, `nom`) VALUES
(1, 'manager'),
(2, 'salarie');

-- --------------------------------------------------------

--
-- Structure de la table `salarie`
--

CREATE TABLE `salarie` (
  `idSalarie` int(60) NOT NULL,
  `nom` varchar(60) NOT NULL,
  `prenom` varchar(60) NOT NULL,
  `email` varchar(60) NOT NULL,
  `mdp` varchar(255) NOT NULL,
  `tauxHoraire` float NOT NULL,
  `nbJourSemaine` float NOT NULL,
  `idRole` int(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `salarie`
--

INSERT INTO `salarie` (`idSalarie`, `nom`, `prenom`, `email`, `mdp`, `tauxHoraire`, `nbJourSemaine`, `idRole`) VALUES
(57, 'will', 'smith', 'will.smith@gmail.com', '$2y$12$vhj2j4iMDn8nBhHtyvyto.UxTNiGY.d/9JRXdiFuou.0Mkg0o7CWO', 38, 5, 2),
(58, 'Sen', 'Eleimane', 'elimane@gmail.com', '$2y$12$gm5J74m8Ony9inkP3KJQUOg94SK8UqwEp5vnp4XCHb/31mLUa4rzW', 38, 5, 1);

-- --------------------------------------------------------

--
-- Structure de la table `salarie_conge`
--

CREATE TABLE `salarie_conge` (
  `idSalarie` int(11) NOT NULL,
  `idConge` int(11) NOT NULL,
  `cumule` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `salarie_conge`
--

INSERT INTO `salarie_conge` (`idSalarie`, `idConge`, `cumule`) VALUES
(57, 1, 33),
(57, 2, 11),
(57, 3, 0),
(57, 4, 6),
(57, 5, 10),
(57, 7, 0),
(57, 9, 0),
(57, 10, 0),
(58, 1, 33),
(58, 2, 11),
(58, 3, 0),
(58, 4, 6),
(58, 5, 10),
(58, 7, 0),
(58, 9, 0),
(58, 10, 0);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `conge`
--
ALTER TABLE `conge`
  ADD PRIMARY KEY (`idConge`);

--
-- Index pour la table `congeacquis`
--
ALTER TABLE `congeacquis`
  ADD PRIMARY KEY (`idCongeAcquis`),
  ADD KEY `idSalarie` (`idSalarie`),
  ADD KEY `idConge` (`idConge`);

--
-- Index pour la table `heuresupp`
--
ALTER TABLE `heuresupp`
  ADD PRIMARY KEY (`idHeureSupp`),
  ADD KEY `idSalarie` (`idSalarie`);

--
-- Index pour la table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `salarie`
--
ALTER TABLE `salarie`
  ADD PRIMARY KEY (`idSalarie`),
  ADD KEY `FK_PersonOrder` (`idRole`);

--
-- Index pour la table `salarie_conge`
--
ALTER TABLE `salarie_conge`
  ADD PRIMARY KEY (`idSalarie`,`idConge`),
  ADD KEY `idConge` (`idConge`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `conge`
--
ALTER TABLE `conge`
  MODIFY `idConge` int(60) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `congeacquis`
--
ALTER TABLE `congeacquis`
  MODIFY `idCongeAcquis` int(60) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=302;

--
-- AUTO_INCREMENT pour la table `heuresupp`
--
ALTER TABLE `heuresupp`
  MODIFY `idHeureSupp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;

--
-- AUTO_INCREMENT pour la table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `salarie`
--
ALTER TABLE `salarie`
  MODIFY `idSalarie` int(60) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `congeacquis`
--
ALTER TABLE `congeacquis`
  ADD CONSTRAINT `congeacquis_ibfk_1` FOREIGN KEY (`idSalarie`) REFERENCES `salarie` (`idSalarie`),
  ADD CONSTRAINT `congeacquis_ibfk_2` FOREIGN KEY (`idConge`) REFERENCES `conge` (`idConge`);

--
-- Contraintes pour la table `heuresupp`
--
ALTER TABLE `heuresupp`
  ADD CONSTRAINT `heuresupp_ibfk_1` FOREIGN KEY (`idSalarie`) REFERENCES `salarie` (`idSalarie`);

--
-- Contraintes pour la table `salarie`
--
ALTER TABLE `salarie`
  ADD CONSTRAINT `FK_PersonOrder` FOREIGN KEY (`idRole`) REFERENCES `role` (`id`);

--
-- Contraintes pour la table `salarie_conge`
--
ALTER TABLE `salarie_conge`
  ADD CONSTRAINT `salarie_conge_ibfk_1` FOREIGN KEY (`idSalarie`) REFERENCES `salarie` (`idSalarie`),
  ADD CONSTRAINT `salarie_conge_ibfk_2` FOREIGN KEY (`idConge`) REFERENCES `conge` (`idConge`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
