
/*****************************validation des formaulair */
let lesFormailaire = document.querySelectorAll('form');
let alertes= document.querySelector('#alert');
let chaine = '';


function validerInput(input)
{
    input.classList.add('is-valid');
    input.classList.remove('is-invalid')
}


function invaliderInput(input)
{
    input.classList.add('is-invalid');
    input.classList.remove('is-valid');
} 


for (let unFormulaire of lesFormailaire )
{
   
   let  lesInput = unFormulaire.querySelectorAll('input','select','textarea');
    unFormulaire.addEventListener('submit', function(event)
    {
        
    for (let unInput of lesInput )
    {
        let inputValide= unInput.validity.valid;
        if(inputValide)
        {
            validerInput(unInput)
        }else{
            invaliderInput(unInput)
        }
    }
    let estValide = unFormulaire.reportValidity();
    if (!estValide)
    {
        
        event.preventDefault();
        chaine= /*html*/`<div class='alert alert-danger'>tous les champs sont requis</div>`
        alertes.innerHTML = chaine;
        return false;
    }
    
})
for (const unInput of lesInput)
{
    unInput.addEventListener('input', function() 
    { 
    let estValid=unInput.validity.valid;
    if (estValid)
    {
        validerInput(unInput)
    }else{
        invaliderInput(unInput)
    }
}
)}
}



